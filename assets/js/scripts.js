// Scripts Globales.
$(document).ready(function(){
    // Activa los tooltip de Bootstrap.
    $('[data-toggle="tooltip"]').tooltip();
    // Da estilo al input tipo file.
    $('#file-picker').filestyle({
        buttonBefore : true,
        size: "md",
        btnClass : 'btn-dark',
        htmlIcon: '<i class="fas fa-images"></i>',
        text: " Seleccione una Imagen",
        placeholder: "No a seleccionado ninguna imagen",
    });
    // Efecto en los alert de bootstrap.
    setTimeout(function() {
        $(".content").fadeOut(1500);
    },3000);
});

// Validaciones de formularios.
$(document).ready(function(){
    // Valida que el correo tenga este formato (user@email.xx o user@email.xxx)
    $.validator.addMethod("email", function(value, element){
        return this.optional(element) || /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i.test(value);
    }, "Ingrese un email valido.");

    // Valida que el campo tenga solo letras y numeros sin espacios.
    $.validator.addMethod("alpha", function(value, element){
        return this.optional(element) || /^[a-zñ]+$/i.test(value);
    }, "Introduzca solo letras sin espacios");

    // Valida que el campo tenga solo numeros sin espacios.
    $.validator.addMethod("num", function(value, element){
        return this.optional(element) || /^[0-9]+$/i.test(value);
    }, "Introduzca solo numeros");

    // Valida el campo tipo file.
    $.validator.addMethod('file-picker', function (value, element, param) {
        return this.optional(element) || (element.files[0].size <= param)
    }, 'No puede exceder de {0} Mb');

    // Formulario de imagen.
    $("#form-img").validate({
        rules:
        {
            userfile: {
                required: true,
                extension: "gif|jpg|png",
                filesize : 2000000   // Tamaño maximo en kb -> 2 Mb.
            },
        },
        messages:
        {
            userfile: {
                required: "Seleccione un imagen",
                extension: "Solo imagenes con formato jpg, gif o png",
                filesize : "No puede exceder de 2 Mb" ,
            },
        },
    });

    // Formulario de Login.
    $("#form-login").validate({
        rules:
        {
            username: {
                required: true,
                alpha: true,
                minlength: 3,
                maxlength: 12,
            },
            password: {
                required: true,
                minlength: 6,
                maxlength: 12,
            },
        },
        messages:
        {
            username: {
                required: "Este campo es requerido",
                alpha: "Introduzca solo letras sin espacios",
                minlength: "Minimo 3 Caracteres",
                maxlength: "Maximo 12 Caracteres",
            },
            password: {
                required: "Este campo es requerido",
                minlength: "Minimo 6 Caracteres",
                maxlength: "Maximo 12 Caracteres",
            },
        },
    });

    // Formulario crear usuario.
    $("#form-add-user").validate({
        rules:
        {
            id_profile: {
                required: true,
            },
            username: {
                required: true,
                alpha: true,
                minlength: 3,
                maxlength: 12,
                remote: {
                    url: "/login_ci/user/validate_user",
                    type: "post",
                }
            },
            name: {
                required: true,
                alpha: true,
                minlength: 3,
                maxlength: 12,
            },
            lastname: {
                required: true,
                alpha: true,
                minlength: 3,
                maxlength: 12,
            },
            email: {
                required: true,
                email: true,
                maxlength: 40,
                remote: {
                    url: "/login_ci/user/validate_email",
                    type: "post",
                }
            },
            id_code: {
                required: true,
                digits: true,
            },
            phone: {
                required: true,
                digits: true,
                minlength: 7,
                maxlength: 7,
            },
            address: {
                required: true,
                minlength: 6,
                maxlength: 100,
            },
            password: {
                required: true,
                minlength: 6,
                maxlength: 12,
            },
            r_password: {
                required: true,
                equalTo: "#password",
            },
        },
        messages:
        {
            id_profile: {
                required: "Este campo es requerido",
            },
            username: {
                required: "Este campo es requerido",
                alpha: "Introduzca solo letras sin espacios",
                minlength: "Minimo 3 Caracteres",
                maxlength: "Maximo 12 Caracteres",
                remote: "Este Usuario ya esta en uso",
            },
            name: {
                required: "Este campo es requerido",
                alpha: "Introduzca solo letras sin espacios",
                minlength: "Minimo 3 Caracteres",
                maxlength: "Maximo 12 Caracteres",
            },
            lastname: {
                required: "Este campo es requerido",
                alpha: "Introduzca solo letras sin espacios",
                minlength: "Minimo 3 Caracteres",
                maxlength: "Maximo 12 Caracteres",
            },
            email: {
                required: "Este campo es requerido",
                maxlength: "Maximo 40 Caracteres",
                email: "Ingrese un email valido",
                remote: "Este correo ya esta en uso"
            },
            id_code: {
                required: "requerido",
                digits: "Introduzca solo numeros",
            },
            phone: {
                required: "Este campo es requerido",
                digits: "Introduzca solo numeros",
                minlength: "Formato 1234567",
                maxlength: "Formato 1234567",
            },
            address: {
                required: "Este campo es requerido",
                minlength: "Minimo 6 Caracteres",
                maxlength: "Maximo 100 Caracteres",
            },
            password: {
                required: "Este campo es requerido",
                minlength: "Minimo 6 Caracteres",
                maxlength: "Maximo 12 Caracteres",
            },
            r_password: {
                required: "Debe repetir su Contraseña",
                equalTo: "La contraseña no coincide",
            },
        },
    });

    // Formulario editar usuario.
    $("#form-edit-user").validate({
        rules:
        {
            id_profile: {
                required: true,
            },
            username: {
                required: true,
                alpha: true,
                minlength: 3,
                maxlength: 12,
            },
            name: {
                required: true,
                alpha: true,
                minlength: 3,
                maxlength: 12,
            },
            lastname: {
                required: true,
                alpha: true,
                minlength: 3,
                maxlength: 12,
            },
            email: {
                required: true,
                email: true,
                maxlength: 40,
            },
            code: {
                required: true,
                digits: true,
            },
            phone: {
                required: true,
                digits: true,
                minlength: 7,
                maxlength: 7,
            },
            address: {
                required: true,
                minlength: 6,
                maxlength: 100,
            },
        },
        messages:
        {
            id_profile: {
                required: "Este campo es requerido",
            },
            username: {
                required: "Este campo es requerido",
                lettersonly: "Introduzca solo letras",
                minlength: "Minimo 3 Caracteres",
                maxlength: "Maximo 12 Caracteres",
            },
            name: {
                required: "Este campo es requerido",
                lettersonly: "Introduzca solo letras",
                minlength: "Minimo 3 Caracteres",
                maxlength: "Maximo 12 Caracteres",
            },
            lastname: {
                required: "Este campo es requerido",
                lettersonly: "Introduzca solo letras",
                minlength: "Minimo 3 Caracteres",
                maxlength: "Maximo 12 Caracteres",
            },
            email: {
                required: "Este campo es requerido",
                maxlength: "Maximo 40 Caracteres",
            },
            code: {
                required: "requerido",
                digits: "Introduzca solo numeros",
            },
            phone: {
                required: "Este campo es requerido",
                digits: "Introduzca solo numeros",
                minlength: "Formato 1234567",
                maxlength: "Formato 1234567",
            },
            address: {
                required: "Este campo es requerido",
                minlength: "Minimo 6 Caracteres",
                maxlength: "Maximo 100 Caracteres",
            },
        },
    });

    // Formulario editar contraseña.
    $("#form-edit-pass").validate({
        rules:
        {
            password: {
                required: true,
                minlength: 6,
                maxlength: 12,
            },
            r_password: {
                required: true,
                equalTo: "#password",
            },
        },
        messages:
        {
            password: {
                required: "Este campo es requerido",
                minlength: "Minimo 6 Caracteres",
                maxlength: "Maximo 12 Caracteres",
            },
            r_password: {
                required: "Debe repetir la contraseña",
                equalTo: "Las contraseñas no coinciden",
            },
        },
    });

});

// Data table.
$(function () {
	$('#table_list_user').DataTable({
		'language':
		{
			"lengthMenu": "Mostrar _MENU_ registros por paginas",
			"zeroRecords": "No se encontraron resultados en su busqueda",
			"searchPlaceholder": "Buscar Usuarios",
			"info": "Mostrando registros del _START_ al _END_ de un total de paginas _PAGE_ de _PAGES_",
			"infoEmpty": "No existen registros",
			"infoFiltered": "(Filtrado de un total de _MAX_ total registros)",
			"search": "Buscar",
			"paginate":
			{
				"first": "Primero",
				"last": "Último",
				"next": "Siguiente",
				"previous": "Anterior",
			}
		}
	});
});

// Ventana modal de confirmacion al ejecutar la accion de editar, eliminar, desactivar y activar usuarios.
$(document).ready(function(){
    // Cambio de contraseña.
    $('a[edit-pass-confirm]').click(function(ev){
        var href = $(this).attr('href');
        if(!$('#password').length){
            $('body').append('<div class="modal fade" id="password" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true"><div class="modal-dialog"><div class="modal-content"><div class="modal-header bg-dark text-white">Contraseña Nueva<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="modal-body text-center">Esta seguro que desea cambiar la Contraseña ?</div><div class="modal-footer"><button type="button" class="btn btn-dark" data-dismiss="modal">Cancelar</button><a class="btn btn-danger text-white" id="pass_edit">Aceptar</a></div></div></div></div>');
        }
        $('#pass_edit').attr('href', href);
        $('#password').modal({show: true});
        return false;
    });
    // Edicion de usuario.
    $('a[edit-user-confirm]').click(function(ev){
        var href = $(this).attr('href');
        if(!$('#user').length){
            $('body').append('<div class="modal fade" id="user" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true"><div class="modal-dialog"><div class="modal-content"><div class="modal-header bg-dark text-white">Editar Usuario<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="modal-body">Esta seguro que desea editar este usuario ?</div><div class="modal-footer"><button type="button" class="btn btn-dark" data-dismiss="modal">Cancelar</button><a class="btn btn-danger text-white" id="edit_user">Aceptar</a></div></div></div></div>');
        }
        $('#edit_user').attr('href', href);
        $('#user').modal({show: true});
        return false;
    });
    // Eliminación de usuario.
    $('a[delete_user_confirm]').click(function(ev){
        var href = $(this).attr('href');
        if(!$('#delete_user').length){
            $('body').append('<div class="modal fade" id="delete_user" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel3" aria-hidden="true"><div class="modal-dialog"><div class="modal-content"><div class="modal-header bg-dark text-white">Eliminar Usuario<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="modal-body">Esta seguro que desea eliminar este usuario ?</div><div class="modal-footer"><button type="button" class="btn btn-dark" data-dismiss="modal">Cancelar</button><a class="btn btn-danger text-white" id="delete">Aceptar</a></div></div></div></div>');
        }
        $('#delete').attr('href', href);
        $('#delete_user').modal({show: true});
        return false;
    });
    // Desactivacion de usuario.
    $('a[disabled-user-confirm]').click(function(ev){
        var href = $(this).attr('href');
        if(!$('#disabled_user').length){
            $('body').append('<div class="modal fade" id="disabled_user" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel3" aria-hidden="true"><div class="modal-dialog"><div class="modal-content"><div class="modal-header bg-dark text-white">Desactivar Usuario<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="modal-body">Esta seguro que desea desactivar este usuario ?</div><div class="modal-footer"><button type="button" class="btn btn-dark" data-dismiss="modal">Cancelar</button><a class="btn btn-danger text-white" id="disable">Aceptar</a></div></div></div></div>');
        }
        $('#disable').attr('href', href);
        $('#disabled_user').modal({show: true});
        return false;
    });
    // Activacion de usuario.
    $('a[enabled-user-confirm]').click(function(ev){
        var href = $(this).attr('href');
        if(!$('#enabled_user').length){
            $('body').append('<div class="modal fade" id="enabled_user" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel3" aria-hidden="true"><div class="modal-dialog"><div class="modal-content"><div class="modal-header bg-dark text-white">Activar Usuario<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="modal-body">Esta seguro que desea activar este usuario ?</div><div class="modal-footer"><button type="button" class="btn btn-dark" data-dismiss="modal">Cancelar</button><a class="btn btn-danger text-white" id="enabled">Aceptar</a></div></div></div></div>');
        }
        $('#enabled').attr('href', href);
        $('#enabled_user').modal({show: true});
        return false;
    });
});
