<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model
{
	public function login_user($username, $password) // Verifica si el usurio existe en la bace de datos.
	{
		$this->db->where('username', $username);
		$this->db->where('password', $password);
		$this->db->where('state', '1');
		$query = $this->db->get('users');
		if($query->num_rows() == 1)
		{
			return $query->row();
		}
		else
		{
			return false;
		}
	}

	public function validate_user($username) // Verifica si un usuario existe.
	{
		$this->db->select('username');
		$this->db->where('username', $username);
		$query = $this->db->get('users');
		$num = $query->num_rows();
		if ($num > 0) {
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	public function validate_email($email) // Verifica si un correo existe.
	{
		$this->db->select('email');
		$this->db->where('email', $email);
		$query = $this->db->get('users');
		$num = $query->num_rows();
		if ($num > 0) {
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	public function get_users() // Consulta todos los usuarios.
	{
		$this->db->select('*');
		$this->db->from('users u');
		$this->db->join('profiles p', 'p.id_profile = u.id_profile');
		$this->db->order_by('u.id_user', 'asc');
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

	public function get_users_admin() // Consulta los usuarios donde perfil sea 2 y 3.
	{
		$this->db->select('*');
		$this->db->from('users u');
		$this->db->join('profiles p', 'p.id_profile = u.id_profile');
		$this->db->group_start();
		$this->db->or_where('p.id_profile', '2');
		$this->db->or_where('p.id_profile', '3');
		$this->db->group_end();
		$this->db->order_by('p.profile', 'asc');
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}


	public function get_users_user() // Consulta los usuarios donde perfil sea 3.
	{
		$this->db->select('*');
		$this->db->from('users u');
		$this->db->join('profiles p', 'p.id_profile = u.id_profile');
		$this->db->group_start();
		$this->db->or_where('p.id_profile', '3');
		$this->db->group_end();
		$this->db->order_by('p.profile', 'asc');
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

	public function get_user($id_user) // Consulta un usuario segun su id.
	{
		$this->db->select('*');
		$this->db->select('date_format(created_user, "%d-%m-%Y %H:%m:%s") AS created_user');
		$this->db->select('date_format(updated_user, "%d-%m-%Y %H:%m:%s") AS updated_user');
		$this->db->select('date_format(updated_pass, "%d-%m-%Y %H:%m:%s") AS updated_pass');
		$this->db->join('profiles', 'profiles.id_profile = users.id_profile');
		$this->db->join('codes', 'codes.id_code = users.id_code');
		$this->db->where('id_user',$id_user);
		$result = $this->db->get('users');
		return $result->row();
	}

	public function get_profiles() // Consulta todos los perfiles.
	{
		$result = $this->db->get('profiles');
		return $result->result();
	}

	public function get_profiles_add() // Consulta los perfiles donde id_profile sea 2 o 3.
	{	$this->db->group_start();
		$this->db->or_where('id_profile', '2');
		$this->db->or_where('id_profile', '3');
		$this->db->group_end();
		$result = $this->db->get('profiles');
		return $result->result();
	}

	public function get_codes() // Consulta todos los codigos de area.
	{
		$result = $this->db->get('codes');
		return $result->result();
	}

	public function add($data) // Inserta un usuario en la tabla users.
	{
		return $this->db->insert('users', $data);
	}

	public function update ($id_user, $data) // Actualiza los datos del usuario segun su id.
	{
		$this->db->where('id_user', $id_user);
		return $this->db->update('users', $data);
	}

	public function disabled($id_user, $data) // Cambia a 0 el estado de un Usuario.
	{
		$this->db->where('id_user', $id_user);
		return $this->db->update('users', $data);
	}

	public function enabled($id_user, $data) // Cambia a 1 el estado de un Usuario.
	{
		$this->db->where('id_user', $id_user);
		return $this->db->update('users', $data);
	}

	public function delete($id_user, $username) // Elimina al segun su id.
	{

		return $this->db->delete('users',array('id_user'=>$id_user));
	}

	public function update_pass($id_user, $data) // Actualiza la contraseña del usuario segun su id.
	{
		$this->db->where('id_user', $id_user);
		return $this->db->update('users', $data);
	}

    public function subir($imagen, $id_user) // Inserta la ruta de la imagen.
    {
   		$array = array(
			'id_user' => $id_user,
            'photo' => $imagen,
            'updated_user' => date('Y-m-d H:i:s'),
		);
		$this->db->set($array);
		$this->db->where('id_user', $id_user);
		$this->db->update('users', $array);
		if ($array)
		{
			$this->session->set_flashdata('msj-success', 'Imagen subida sastifactoriamente !!!');
		}
		else
		{
			return false;
		}
    }
}

/* End of file User_model.php */
/* Location: ./application/models/User_model.php */
