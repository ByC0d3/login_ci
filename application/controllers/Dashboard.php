<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		if($this->session->userdata('id_profile')) // Si existe una session activa carga la vista dashboar.
		{
			$data = array(
				'title' 	=> '-- Dashboard --', // Envia el titulo a la vista dashboard.
       			'bg_body' 	=> '', // Envia el nombre de la clase que contiene el fondo del body, se puede fijar un estilo diferente para cada vista.
       			'header' 	=> 'layout/header',
       			'content'	=> 'dashboard/dashboard',
       			'footer' 	=> 'layout/footer',
       		);
			$this->load->view('layout/template', $data);
		}
		else // Si no existe ninguna session reenvia a la vista login.
		{
			redirect(base_url('login'));
		}
	}

}

/* End of file Dashboard.php */
/* Location: ./application/controllers/Dashboard.php */
