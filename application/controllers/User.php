<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('User_model');
		// Si no exite ninguna session activa, envia al usuario al login.
		if(!$this->session->userdata('id_profile')){
			redirect(base_url('login/login_form'));
		};
	}

	public function index() // Lista los usuarios.
	{
		if ($this->session->userdata('id_profile') === '1') // system.
		{
			$data = array(
				'users'   => $this->User_model->get_users(),
				'title'   => '-- System - User --',
				'bg_body' => '',
				'header'  => 'layout/header',
				'content' => 'user/list',
				'footer'  => 'layout/footer',
			);
			$this->load->view('layout/template', $data);
		}
		elseif ($this->session->userdata('id_profile') === '2') // admin.
		{
			$data = array(
				'users'   => $this->User_model->get_users_admin(),
				'title'   => '-- Admin - User --',
				'bg_body' => '',
				'header'  => 'layout/header',
				'content' => 'user/list',
				'footer'  => 'layout/footer',
			);
			$this->load->view('layout/template', $data);
		}
		elseif ($this->session->userdata('id_profile') === '3') // usuario.
		{
			$data = array(
				'users'   => $this->User_model->get_users_user(),
				'title'   => '-- Admin - User --',
				'bg_body' => '',
				'header'  => 'layout/header',
				'content' => 'user/list',
				'footer'  => 'layout/footer',
			);
			$this->load->view('layout/template', $data);
		}
		else // Cualquier otro perfil fuera de los tres principales.
		{
			redirect(base_url('login/login_form'));
		}
	}

	/*** Valida si el usuario existe antes de guardarlo ***/
	public function validate_user()
	{
		if (array_key_exists('username', $_POST))
		{
			if ($this->User_model->validate_user($this->input->post('username'))==TRUE)
			{
				echo json_encode(FALSE);
			}
			else
			{
				echo json_encode(TRUE);
			}
		}
	}

	/*** Valida si el correo existe antes de guardarlo ***/
	public function validate_email()
	{
		if (array_key_exists('email', $_POST))
		{
			if ($this->User_model->validate_email($this->input->post('email'))==TRUE)
			{
				echo json_encode(FALSE);
			}
			else
			{
				echo json_encode(TRUE);
			}
		}
	}

	public function add() // Agregar usuarios
	{
		if ($this->session->userdata('id_profile') === '1') // system.
		{
			$data = array(
				'get_profiles' => $this->User_model->get_profiles_add(),
				'get_codes'	   => $this->User_model->get_codes(),
				'title' 	   => '-- System - Add User --',
				'bg_body' 	   => '',
				'header' 	   => 'layout/header',
				'content'	   => 'user/add',
				'footer' 	   => 'layout/footer',
			);
			$this->load->view('layout/template', $data);
		}
		elseif ($this->session->userdata('id_profile') === '2') // admin.
		{
			$data = array(
				'get_profiles' => $this->User_model->get_profiles_add(),
				'get_codes'	   => $this->User_model->get_codes(),
				'title' 	   => '-- Admin - Add User --',
				'bg_body' 	   => '',
				'header' 	   => 'layout/header',
				'content'	   => 'user/add',
				'footer' 	   => 'layout/footer',
			);
			$this->load->view('layout/template', $data);
		}
		elseif ($this->session->userdata('id_profile') === '3') // usuario.
		{
			$this->session->set_flashdata('msj_error', 'No tienes privilegios para agregar Usuarios !!!');
			redirect(base_url('user'));
		}
		else // Cualquier otro perfil fuera de los tres principales.
		{
			redirect(base_url('login/logout'));
		}
	}

	public function store() // Recibe, valida y envia al modelo users los datos del nuevo usuario.
	{
		// Recibe los datos enviados del formulario
		$username 	= $this->input->post('username');
		$name 		= $this->input->post('name');
		$lastname 	= $this->input->post('lastname');
		$email 		= $this->input->post('email');
		$id_code	= $this->input->post('id_code');
		$phone		= $this->input->post('phone');
		$address	= $this->input->post('address');
		$password 	= $this->input->post('password');
		$id_profile = $this->input->post('id_profile');

		if ($this->session->userdata('id_profile') === '1') // system.
		{
			// Validaciones del formulario con la librería form_validation
			$this->form_validation->set_rules('id_profile', 'Profile', 'trim|required|strip_tags|xss_clean|in_list[1,2,3]',
				array(
					'required' => 'Este campo es requerido.',
					'in_list'  => 'Perfil incorrecto.',
				));
			$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[3]|max_length[12]|strip_tags|xss_clean|alpha_numeric|is_unique[users.username]',
				array(
					'required'	 	=> 'Este campo es requerido.',
					'min_length' 	=> 'Minimo 3 Caracteres.',
					'max_length' 	=> 'Maximo 12 Caracteres',
					'alpha_numeric'	=> 'Introduzca solo letras y numeros.',
					'is_unique'	 	=> 'Este Usuario ya esta en uso.',
				));
			$this->form_validation->set_rules('name', 'Nombre', 'trim|required|min_length[3]|max_length[12]|strip_tags|xss_clean|alpha',
				array(
					'required'	 => 'Este campo es requerido.',
					'min_length' => 'Minimo 3 Caracteres.',
					'max_length' => 'Maximo 12 Caracteres',
					'alpha'		 => 'Introduzca solo letras.',
				));
			$this->form_validation->set_rules('lastname', 'Apellido', 'trim|required|min_length[3]|max_length[12]|strip_tags|xss_clean|alpha',
				array(
					'required'	 => 'Este campo es requerido',
					'min_length' => 'Minimo 3 Caracteres',
					'max_length' => 'Maximo 12 Caracteres',
					'alpha'		 => 'Introduzca solo letra',
				));
			$this->form_validation->set_rules('email', 'Correo', 'trim|required|valid_email|max_length[40]|strip_tags|xss_clean|is_unique[users.email]',
				array(
					'required'	  => 'Este campo es requerido',
					'valid_email' => 'Ingrese un correo valido',
					'max_length'  => 'Maximo 40 Caracteres',
					'is_unique'	  => 'Este Correo ya esta en uso',
				));
			$this->form_validation->set_rules('id_code', 'Codigo', 'trim|required|numeric|in_list[1,2,3,4,5]|strip_tags|xss_clean',
				array(
					'required' => 'Este campo es requerido.',
					'numeric'  => 'Introduzca solo numeros.',
					'in_list'  => 'Codigo incorrecto.',
				));
			$this->form_validation->set_rules('phone', 'Telefono', 'trim|required|numeric|min_length[7]|max_length[7]|strip_tags|xss_clean',
				array(
					'required'	 => 'Este campo es requerido',
					'numeric'    => 'Introduzca solo numeros.',
					'min_length' => 'Formato 1234567',
					'max_length' => 'Formato 1234567',
				));
			$this->form_validation->set_rules('address', 'Direccion', 'trim|required|min_length[6]|max_length[100]|strip_tags|xss_clean',
				array(
					'required'	 => 'Este campo es requerido.',
					'min_length' => 'Minimo 6 Caracteres',
					'max_length' => 'Maximo 100 Caracteres',
				));
			$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]|max_length[12]|strip_tags|xss_clean',
				array(
					'required'	 => 'Este campo es requerido.',
					'min_length' => 'Minimo 6 Caracteres.',
					'max_length' => 'Maximo 12 Caracteres',
				)
			);
			$this->form_validation->set_rules('r_password', 'Password', 'trim|required|matches[password]|strip_tags|xss_clean',
				array(
					'required' => 'Este campo es requerido',
					'matches'  => 'Las contraseñas no coinciden',
				)
			);
			if ($this->form_validation->run() == FALSE)
			{
				$this->add();
			}
			else
			{
				$data = array(
					'id_profile'   => $id_profile,
					'state'		   => '1',
					'username' 	   => strtolower($username),
					'name' 		   => strtolower($name),
					'lastname' 	   => strtolower($lastname),
					'photo' 	   => "../img_profile.jpg",
					'email' 	   => strtolower($email),
					'id_code'  	   => $id_code,
					'phone' 	   => $phone,
					'address' 	   => strtolower($address),
					'password'	   => sha1($password),
				);
				if ($this->User_model->add($data))
				{
					$this->session->set_flashdata('msj_success', 'El Usuario se guardo correctamente !!!');
					mkdir('./assets/profile/'.$username);
					redirect(base_url('user'));
				}
				else
				{
					$this->session->set_flashdata('msj_error', 'No se guardo el Usuario');
					redirect(base_url('user/add'));
				}
			}
		}
		elseif ($this->session->userdata('id_profile') === '2') // admin.
		{
			// Validaciones del formulario con la librería form_validation
			$this->form_validation->set_rules('id_profile', 'Profile', 'trim|required|strip_tags|xss_clean|in_list[2,3]',
				array(
					'required' => 'Este campo es requerido.',
					'in_list'  => 'Perfil incorrecto.',
				));
			$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[3]|max_length[12]|strip_tags|xss_clean|alpha|is_unique[users.username]',
				array(
					'required'	 => 'Este campo es requerido.',
					'min_length' => 'Minimo 3 Caracteres.',
					'max_length' => 'Maximo 12 Caracteres',
					'alpha'		 => 'Introduzca solo letras.',
					'is_unique'	 => 'Nombre de Usuario en uso.',
				));
			$this->form_validation->set_rules('name', 'Nombre', 'trim|required|min_length[3]|max_length[12]|strip_tags|xss_clean|alpha',
				array(
					'required'	 => 'Este campo es requerido.',
					'min_length' => 'Minimo 3 Caracteres.',
					'max_length' => 'Maximo 12 Caracteres',
					'alpha'		 => 'Introduzca solo letras.',
				));
			$this->form_validation->set_rules('lastname', 'Apellido', 'trim|required|min_length[3]|max_length[12]|strip_tags|xss_clean|alpha',
				array(
					'required'	 => 'Este campo es requerido.',
					'min_length' => 'Minimo 3 Caracteres.',
					'max_length' => 'Maximo 12 Caracteres',
					'alpha'		 => 'Introduzca solo letras.',
				));
			$this->form_validation->set_rules('email', 'Correo', 'trim|required|valid_email|max_length[40]|strip_tags|xss_clean|is_unique[users.email]',
				array(
					'required'	  => 'Este campo es requerido.',
					'valid_email' => 'Ingrese un correo valido.',
					'max_length'  => 'Maximo 40 Caracteres',
					'is_unique'	  => 'Este correo ya esta en uso.',
				));
			$this->form_validation->set_rules('id_code', 'Codigo', 'trim|required|numeric|in_list[1,2,3,4,5]|strip_tags|xss_clean',
				array(
					'required' => 'Este campo es requerido.',
					'numeric'  => 'Introduzca solo numeros.',
					'in_list'  => 'Codigo incorrecto.',
				));
			$this->form_validation->set_rules('phone', 'Telefono', 'trim|required|numeric|min_length[7]|max_length[7]|strip_tags|xss_clean',
				array(
					'required'	 => 'Este campo es requerido',
					'numeric'  	 => 'Introduzca solo numeros.',
					'min_length' => 'Formato 1234567',
					'max_length' => 'Formato 1234567',
				));
			$this->form_validation->set_rules('address', 'Direccion', 'trim|required|min_length[6]|max_length[100]|strip_tags|xss_clean',
				array(
					'required'	 => 'Este campo es requerido.',
					'min_length' => 'Minimo 6 Caracteres',
					'max_length' => 'Maximo 100 Caracteres',
				));
			$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]|max_length[12]|strip_tags|xss_clean',
				array(
					'required'	 => 'Este campo es requerido.',
					'min_length' => 'Minimo 6 Caracteres.',
					'max_length' => 'Maximo 12 Caracteres',
				)
			);
			$this->form_validation->set_rules('r_password', 'Password', 'trim|required|matches[password]|strip_tags|xss_clean',
				array(
					'required' => 'Este campo es requerido.',
					'matches'  => 'Las contraseñas no coinciden.',
				)
			);
			if ($this->form_validation->run() == FALSE)
			{
				$this->add();
			}
			else
			{
				$data = array(
					'id_profile'   => $id_profile,
					'state'		   => '1',
					'username' 	   => strtolower($username),
					'name' 		   => strtolower($name),
					'lastname' 	   => strtolower($lastname),
					'photo' 	   => "../img_profile.jpg",
					'email' 	   => strtolower($email),
					'id_code'  	   => $id_code,
					'phone' 	   => $phone,
					'address' 	   => strtolower($address),
					'password'	   => sha1($password),
				);
				if ($this->User_model->add($data))
				{
					$this->session->set_flashdata('msj_success', 'El Usuario se guardo correctamente !!!');
					mkdir('./assets/profile/'.$username);
					redirect(base_url('user'));
				}
				else
				{
					$this->session->set_flashdata('msj_error', 'No se guardo el Usuario');
					redirect(base_url('user/add'));
				}
			}
		}
		elseif ($this->session->userdata('id_profile') === '3') // usuario.
		{
			$this->session->set_flashdata('msj_error', 'No tienes privilegios para agregar Usuarios !!!');
			redirect(base_url('dashboard'));
		}
		else //Cualquier otro perfil fuera de los tres principales.
		{
			redirect(base_url('login/login_form'));
		}
	}

	public function edit($id_user, $id_profile)
	{
		$this->session->set_flashdata('id_profile_edit', $id_profile);
		if ($this->session->userdata('id_profile') === '1') // system.
		{
			if ($id_profile == '1')
			{
				$this->session->set_flashdata('msj_error', 'No se puede modificar un Usuario predeterminado !!!');
				redirect(base_url('dashboard'));
			}
			else
			{
				$data = array(
					'user'		   => $this->User_model->get_user($id_user),
					'get_profiles' => $this->User_model->get_profiles_add(),
					'get_codes'    => $this->User_model->get_codes(),
					'title'        => '-- Edit User --',
					'bg_body' 	   => '',
					'header' 	   => 'layout/header',
					'content'	   => 'user/edit',
					'footer' 	   => 'layout/footer',
				);
				$this->load->view('layout/template', $data);
			}
		}
		elseif ($this->session->userdata('id_profile') === '2') // admin.
		{
			if ($id_profile == '1')
			{
				$this->session->set_flashdata('msj_error', 'No se puede modificar un Usuario predeterminado !!!');
				redirect(base_url('dashboard'));
			}
			else
			{
				$data = array(
					'user' 				 => $this->User_model->get_user($id_user),
					'get_profiles_admin' => $this->User_model->get_profiles_add(),
					'get_codes'   	 	 => $this->User_model->get_codes(),
					'title' 			 => '-- Edit User --',
					'bg_body' 			 => '',
					'header' 			 => 'layout/header',
					'content'			 => 'user/edit',
					'footer' 			 => 'layout/footer',
				);
				$this->load->view('layout/template', $data);
			}
		}
		elseif ($this->session->userdata('id_profile') === '3') // usuario.
		{
			if ($this->session->userdata('id_user') == $id_user) // Si el id es igual puede editar su Usuario.
			{
				$data = array(
					'user' 	  	=> $this->User_model->get_user($id_user),
					'get_codes' => $this->User_model->get_codes(),
					'title'   	=> '-- Edit User --',
					'bg_body'	=> '',
					'header'  	=> 'layout/header',
					'content' 	=> 'user/edit',
					'footer'  	=> 'layout/footer',
				);
				$this->load->view('layout/template', $data);
			}
			else
			{
				$this->session->set_flashdata('msj_error', 'No puedes editar otro Usuario !!!');
				redirect(base_url('dashboard'));
			}
		}
		else //Cualquier otro perfil fuera de los tres principales.
		{
			redirect(base_url('login/login_form'));
		}
	}

	public function update()
	{
		if($this->session->userdata('id_profile')) // Verifica si existe una sessión activa.
		{
			// Recibe los datos enviados del formulario
			$id_user 	= $this->input->post('id_user');
			$id_profile	= $this->input->post('id_profile');
			$username 	= $this->input->post('username');
			$name 		= $this->input->post('name');
			$lastname 	= $this->input->post('lastname');
			$email 		= $this->input->post('email');
			$id_code 	= $this->input->post('id_code');
			$phone 		= $this->input->post('phone');
			$address 	= $this->input->post('address');

			if ($this->session->userdata('id_profile') === '1') // System.
			{
				// Si el usuario es el mismo que se esta editando no comprara si es unico.
				$username_current = $this->User_model->get_user($id_user);
				if ($username == $username_current->username)
				{
					$is_unique_username = '';
				}
				else
				{
					$is_unique_username = '|is_unique[users.username]';
				}
				// Si el correo es el mismo que se esta editando no comprara si es unico.
				$email_current = $this->User_model->get_user($id_user);
				if ($email == $email_current->email)
				{
					$is_unique_email = '';
				}
				else
				{
					$is_unique_email = '|is_unique[users.email]';
				}
				// Validaciones del formulario con la librería form_validation
				$this->form_validation->set_rules('id_profile', 'Profile', 'trim|required|strip_tags|xss_clean|in_list[1,2,3]',
					array(
						'required' => 'Este campo es requerido.',
						'in_list'  => 'Perfil incorrecto.',
					));
				$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[3]|max_length[12]|strip_tags|xss_clean|alpha'.$is_unique_username,
					array(
						'required'	 => 'Este campo es requerido.',
						'min_length' => 'Minimo 3 Caracteres.',
						'max_length' => 'Maximo 12 Caracteres',
						'alpha'		 => 'Introduzca solo letras.',
						'is_unique'	 => 'Nombre de Usuario en uso.',
					));
				$this->form_validation->set_rules('name', 'Nombre', 'trim|required|min_length[3]|max_length[12]|strip_tags|xss_clean|alpha',
					array(
						'required'	 => 'Este campo es requerido.',
						'min_length' => 'Minimo 3 Caracteres.',
						'max_length' => 'Maximo 12 Caracteres',
						'alpha'		 => 'Introduzca solo letras.',
					));
				$this->form_validation->set_rules('lastname', 'Apellido', 'trim|required|min_length[3]|max_length[12]|strip_tags|xss_clean|alpha',
					array(
						'required'	 => 'Este campo es requerido.',
						'min_length' => 'Minimo 3 Caracteres.',
						'max_length' => 'Maximo 12 Caracteres',
						'alpha'		 => 'Introduzca solo letras.',
					));
				$this->form_validation->set_rules('email', 'Correo', 'trim|required|valid_email|max_length[40]|strip_tags|xss_clean'.$is_unique_email,
					array(
						'required'	  => 'Este campo es requerido.',
						'valid_email' => 'Ingrese un correo valido.',
						'max_length'  => 'Maximo 40 Caracteres',
						'is_unique'   => 'Este correo ya esta en uso.',
					));
				$this->form_validation->set_rules('id_code', 'Codigo', 'trim|required|numeric|in_list[1,2,3,4,5]|strip_tags|xss_clean',
					array(
						'required' => 'Este campo es requerido.',
						'numeric'  => 'Introduzca solo numeros.',
						'in_list'  => 'Codigo incorrecto.',
					));
				$this->form_validation->set_rules('phone', 'Telefono', 'trim|required|numeric|min_length[7]|max_length[7]|strip_tags|xss_clean',
					array(
						'required'	 => 'Este campo es requerido',
						'numeric'  => 'Introduzca solo numeros.',
						'min_length' => 'Formato 1234567',
						'max_length' => 'Formato 1234567',
					));
				$this->form_validation->set_rules('address', 'Direccion', 'trim|required|min_length[6]|max_length[100]|strip_tags|xss_clean',
					array(
						'required'	=> 'Este campo es requerido.',
						'min_length'=> 'Minimo 6 Caracteres',
						'max_length'=> 'Maximo 100 Caracteres',
					));
				if ($this->form_validation->run() == FALSE)
				{
					$this->edit($id_user, $id_profile = $this->session->flashdata('id_profile_edit'));
				}
				else
				{
					$data = array(
						'id_user'	   => $id_user,
						'id_profile'   => $id_profile,
						'username' 	   => strtolower($username),
						'name' 		   => strtolower($name),
						'lastname' 	   => strtolower($lastname),
						'email' 	   => strtolower($email),
						'id_code' 	   => $id_code,
						'phone' 	   => $phone,
						'address' 	   => strtolower($address),
						'updated_user' => date('Y-m-d H:i:s'),
					);
					if ($this->User_model->update($id_user, $data))
					{
						$this->session->set_flashdata('msj-success', 'El Usuario se actualizó correctamente !!!');
						$this->profile($id_user);
					}
					else
					{
						$this->session->set_flashdata('msj_error', 'No se actualizó el Usuario !!!');
						redirect(base_url('user'));
					}
				}
			}
			elseif ($this->session->userdata('id_profile') === '2') // admin.
			{
				// Si el usuario es el mismo que se esta editando no comprara si es unico.
				$username_current = $this->User_model->get_user($id_user);
				if ($username == $username_current->username)
				{
					$is_unique_username = '';
				}
				else
				{
					$is_unique_username = '|is_unique[users.username]';
				}
				// Si el correo es el mismo que se esta editando no comprara si es unico.
				$email_current =$this->User_model->get_user($id_user);
				if ($email == $email_current->email)
				{
					$is_unique_email = '';
				}
				else
				{
					$is_unique_email = '|is_unique[users.email]';
				}
				// Validaciones del formulario con la librería form_validation
				$this->form_validation->set_rules('id_profile', 'Profile', 'trim|required|strip_tags|xss_clean|in_list[2,3]',
					array(
						'required' => 'Este campo es requerido.',
						'in_list'  => 'Perfil incorrecto.',
					));
				$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[3]|max_length[12]|strip_tags|xss_clean|alpha'.$is_unique_username,
					array(
						'required'	 => 'Este campo es requerido.',
						'min_length' => 'Minimo 3 Caracteres.',
						'max_length' => 'Maximo 12 Caracteres',
						'in_list'  	 => 'Perfil incorrecto.',
						'alpha'		 => 'Introduzca solo letras.',
						'is_unique'	 => 'Nombre de Usuario en uso.',
					));
				$this->form_validation->set_rules('name', 'Nombre', 'trim|required|min_length[3]|max_length[12]|strip_tags|xss_clean|alpha',
					array(
						'required'	 => 'Este campo es requerido.',
						'min_length' => 'Minimo 3 Caracteres.',
						'max_length' => 'Maximo 12 Caracteres',
						'alpha'		 => 'Introduzca solo letras.',
					));
				$this->form_validation->set_rules('lastname', 'Apellido', 'trim|required|min_length[3]|max_length[12]|strip_tags|xss_clean|alpha',
					array(
						'required'	=> 'Este campo es requerido.',
						'min_length'=> 'Minimo 3 Caracteres.',
						'max_length'=> 'Maximo 12 Caracteres',
						'alpha'		=> 'Introduzca solo letras.',
					));
				$this->form_validation->set_rules('email', 'Correo', 'trim|required|valid_email|max_length[40]|strip_tags|xss_clean'.$is_unique_email,
					array(
						'required'	  => 'Este campo es requerido.',
						'valid_email' => 'Ingrese un correo valido.',
						'max_length'  => 'Maximo 40 Caracteres',
						'is_unique'   => 'Este correo ya esta en uso.',
					));
				$this->form_validation->set_rules('id_code', 'Codigo', 'trim|required|numeric|in_list[1,2,3,4,5]|strip_tags|xss_clean',
					array(
						'required' => 'Este campo es requerido.',
						'numeric'  => 'Introduzca solo numeros.',
						'in_list'  => 'Codigo incorrecto.',
					));
				$this->form_validation->set_rules('phone', 'Telefono', 'trim|required|numeric|min_length[7]|max_length[7]|strip_tags|xss_clean',
					array(
						'required'	 => 'Este campo es requerido',
						'numeric'  => 'Introduzca solo numeros.',
						'min_length' => 'Formato 1234567',
						'max_length' => 'Formato 1234567',
					));
				$this->form_validation->set_rules('address', 'Direccion', 'trim|min_length[6]|max_length[100]|strip_tags|xss_clean',
					array(
						'min_length' 		   => 'Minimo 6 Caracteres.',
						'max_length'		   => 'Maximo 100 Caracteres',
					));
				if ($this->form_validation->run() == FALSE)
				{
					$this->edit($id_user, $id_profile = $this->session->flashdata('id_profile_edit'));
				}
				else
				{
					$data = array(
						'id_user'	   => $id_user,
						'id_profile'   => $id_profile,
						'username' 	   => strtolower($username),
						'name' 		   => strtolower($name),
						'lastname' 	   => strtolower($lastname),
						'email' 	   => strtolower($email),
						'id_code' 	   => $id_code,
						'phone' 	   => $phone,
						'address' 	   => strtolower($address),
						'updated_user' => date('Y-m-d H:i:s'),
					);
					if ($this->User_model->update($id_user, $data))
					{
						$this->session->set_flashdata('msj-success', 'El Usuario se actualizó correctamente !!!');
						redirect(base_url('user'));
					}
					else
					{
						$this->session->set_flashdata('msj_error', 'No se actualizó el Usuario !!!');
						redirect(base_url('user'));
					}
				}
			}
			elseif ($this->session->userdata('id_profile') === '3') // usuario.
			{
				// Si el usuario es el mismo que se esta editando no comprara si es unico.
				$username_current = $this->User_model->get_user($id_user);
				if ($username == $username_current->username)
				{
					$is_unique_username = '';
				}
				else
				{
					$is_unique_username = '|is_unique[users.username]';
				}
				// Si el correo es el mismo que se esta editando no comprara si es unico.
				$email_current = $this->User_model->get_user($id_user);
				if ($email == $email_current->email)
				{
					$is_unique_email = '';
				}
				else
				{
					$is_unique_email = '|is_unique[users.email]';
				}
				// Validaciones del formulario con la librería form_validation
				$this->form_validation->set_rules('id_profile', 'Profile', 'trim|required|strip_tags|xss_clean|in_list[3]',
					array(
						'required' => 'Este campo es requerido.',
						'in_list'  => 'Solo Usuarios.',
					));
				$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[3]|max_length[12]|strip_tags|xss_clean|alpha'.$is_unique_username,
					array(
						'required'	 => 'Este campo es requerido.',
						'min_length' => 'Minimo 3 Caracteres.',
						'max_length' => 'Maximo 12 Caracteres',
						'alpha'		 => 'Introduzca solo letras.',
						'is_unique'	 => 'Nombre de Usuario en uso.',
					));
				$this->form_validation->set_rules('name', 'Nombre', 'trim|required|min_length[3]|max_length[12]|strip_tags|xss_clean|alpha',
					array(
						'required'	 => 'Este campo es requerido.',
						'min_length' => 'Minimo 3 Caracteres.',
						'max_length' => 'Maximo 12 Caracteres',
						'alpha'		 => 'Introduzca solo letras.',
					));
				$this->form_validation->set_rules('lastname', 'Apellido', 'trim|required|min_length[3]|max_length[12]|strip_tags|xss_clean|alpha',
					array(
						'required'	 => 'Este campo es requerido.',
						'min_length' => 'Minimo 3 Caracteres.',
						'max_length' => 'Maximo 12 Caracteres',
						'alpha'		 => 'Introduzca solo letras.',
					));
				$this->form_validation->set_rules('email', 'Correo', 'trim|required|valid_email|max_length[40]|strip_tags|xss_clean'.$is_unique_email,
					array(
						'required'	  => 'Este campo es requerido.',
						'valid_email' => 'Ingrese un correo valido.',
						'max_length'  => 'Maximo 40 Caracteres',
						'is_unique'	  => 'Este correo ya esta en uso.',
					));
				$this->form_validation->set_rules('id_code', 'Codigo', 'trim|required|numeric|in_list[1,2,3,4,5]|strip_tags|xss_clean',
					array(
						'required' => 'Este campo es requerido.',
						'numeric'  => 'Introduzca solo numeros.',
						'in_list'  => 'Codigo incorrecto.',
					));
				$this->form_validation->set_rules('phone', 'Telefono', 'trim|required|numeric|min_length[7]|max_length[7]|strip_tags|xss_clean',
					array(
						'required'	 => 'Este campo es requerido',
						'numeric'  => 'Introduzca solo numeros.',
						'min_length' => 'Formato 1234567',
						'max_length' => 'Formato 1234567',
					));
				$this->form_validation->set_rules('address', 'Direccion', 'trim|required|min_length[6]|max_length[100]|strip_tags|xss_clean',
					array(
						'required'	=> 'Este campo es requerido.',
						'min_length'=> 'Minimo 6 Caracteres',
						'max_length'=> 'Maximo 100 Caracteres',
					));
				if ($this->form_validation->run() == FALSE)
				{
					$this->edit($id_user, $id_profile = $this->session->flashdata('id_profile_edit'));
				}
				else
				{
					$data = array(
						'id_user'	   => $id_user,
						'id_profile'   => $id_profile,
						'username' 	   => strtolower($username),
						'name' 		   => strtolower($name),
						'lastname' 	   => strtolower($lastname),
						'email' 	   => strtolower($email),
						'id_code' 	   => $id_code,
						'phone' 	   => $phone,
						'address' 	   => strtolower($address),
						'updated_user' => date('Y-m-d H:i:s'),
					);
					if ($this->User_model->update($id_user, $data))
					{
						$this->session->set_flashdata('msj-success', 'El Usuario se actualizó correctamente !!!');
						$this->profile($id_user);
					}
					else
					{
						$this->session->set_flashdata('msj_error', 'No se actualizó el Usuario !!!');
						redirect(base_url('dashboard'));
					}
				}
			}
			else //Cualquier otro perfil fuera de los tres principales.
			{
				redirect(base_url('login/login_form'));
			}
		}
		else // Si no existe ninguna session reenvia a la vista login.
		{
			redirect(base_url('login/login_form'));
		}
	}

	public function profile($id_user) // Lista los usuarios.
	{
		if ($this->session->userdata('id_profile') === '1') // system.
		{
			$data = array(
				'user'    => $this->User_model->get_user($id_user),
				'title'   => '-- System - User --',
				'bg_body' => '',
				'header'  => 'layout/header',
				'content' => 'user/profile',
				'footer'  => 'layout/footer',
			);
			$this->load->view('layout/template', $data);
		}
		elseif ($this->session->userdata('id_profile') === '2') // admin.
		{
			$data = array(
				'user'    => $this->User_model->get_user($id_user),
				'title'   => '-- Admin - User --',
				'bg_body' => '',
				'header'  => 'layout/header',
				'content' => 'user/profile',
				'footer'  => 'layout/footer',
			);
			$this->load->view('layout/template', $data);
		}
		elseif ($this->session->userdata('id_profile') === '3') // usuario.
		{
			if ($this->session->userdata('id_user') === $id_user)
			{
				$data = array(
					'user'    => $this->User_model->get_user($id_user),
					'title'   => '-- Admin - User --',
					'bg_body' => '',
					'header'  => 'layout/header',
					'content' => 'user/profile',
					'footer'  => 'layout/footer',
				);
				$this->load->view('layout/template', $data);
			}
			else
			{
				$this->session->set_flashdata('msj_error', 'No puedes ver el perfil de otros Usuarios !!!');
				redirect(base_url('dashboard'));
			}
		}
		else // Cualquier otro perfil fuera de los tres principales.
		{
			redirect(base_url('login/login_form'));
		}
	}

	public function disabled($id_user, $id_profile)
	{
		if ($this->session->userdata('id_profile') === '1') // system.
		{
			if ($id_profile === '1')
			{
				$this->session->set_flashdata('msj_error', 'No puedes desactivar a Usuarios predeterminados del sistema !!!');
				redirect(base_url('dashboard'));
			}
			else
			{
				if ($this->session->userdata('id_user') === $id_user)
				{
					$this->session->set_flashdata('msj_error', 'No puedes desactivar el Usuario con el que estas logueado !!!');
					redirect(base_url('user'));
				}
				else
				{
					$data = array(
						'user' => $this->User_model->get_user($id_user),
					);
					if (isset($data['user']))
					{
						$data = array(
							'state' => '0',
							'updated_user' => date('Y-m-d H:i:s'),
						);
						$this->User_model->disabled($id_user, $data);
						$this->session->set_flashdata('msj_success', 'El Usuario se desactivado correctamente !!!');
						redirect(base_url('user'));
					}
					else
					{
						$this->session->set_flashdata('msj_error', 'El Usuario no existe !!!');
						redirect(base_url('user'));
					}
				}
			}
		}
		elseif ($this->session->userdata('id_profile') === '2') // admin.
		{
			if ($id_profile === '1')
			{
				$this->session->set_flashdata('msj_error', 'No puedes desactivar un Usuario del Sistema !!!');
				redirect(base_url('dashboard'));
			}
			else
			{
				if ($this->session->userdata('id_user') === $id_user)
				{
					$this->session->set_flashdata('msj_error', 'No puedes desactivar el Usuario con el que estas logueado !!!');
					redirect(base_url('user'));
				}
				else
				{
					$data = array(
						'user' => $this->User_model->get_user($id_user),
					);
					if (isset($data['user']))
					{
						$data = array(
							'state' => '0',
							'updated_user' => date('Y-m-d H:i:s'),
						);
						$this->User_model->disabled($id_user, $data);
						$this->session->set_flashdata('msj_success', 'El Usuario se desactivo correctamente !!!');
						redirect(base_url('user'));
					}
					else
					{
						$this->session->set_flashdata('msj_error', 'El Usuario no existe !!!');
						redirect(base_url('user'));
					}
				}
			}
		}
		elseif ($this->session->userdata('id_profile') === '3') // usuario.
		{
			$this->session->set_flashdata('msj_error', 'No tiene privilegios desactivar a un Usuario !!!');
			redirect(base_url('dashboard'));
		}
		else //Cualquier otro perfil fuera de los tres principales.
		{
			redirect(base_url('login/login_form'));
		}
	}

	public function enabled($id_user, $id_profile)
	{
		if ($this->session->userdata('id_profile') === '1') // system.
		{
			if ($id_profile === '1')
			{
				$this->session->set_flashdata('msj_error', 'No puedes activar a Usuarios predeterminados del Sistema !!!');
				redirect(base_url('dashboard'));
			}
			else
			{
				if ($this->session->userdata('id_user') === $id_user)
				{
					$this->session->set_flashdata('msj_error', 'No puedes activar el Usuario con el que estas logueado !!!');
					redirect(base_url('user'));
				}
				else
				{
					$data = array(
						'user' => $this->User_model->get_user($id_user),
					);
					if (isset($data['user']))
					{
						$data = array(
							'state' => '1',
						);
						$this->User_model->enabled($id_user, $data);
						$this->session->set_flashdata('msj_success', 'El Usuario se activo correctamente !!!');
						redirect(base_url('user'));
					}
					else
					{
						$this->session->set_flashdata('msj_error', 'El Usuario no existe !!!');
						redirect(base_url('user'));
					}
				}
			}
		}
		elseif ($this->session->userdata('id_profile') === '2') // admin.
		{
			if ($id_profile === '1') {
				$this->session->set_flashdata('msj_error', 'No puedes activar a Usuarios predeterminados del Sistema !!!');
				redirect(base_url('dashboard'));
			}
			else
			{
				if ($this->session->userdata('id_user') === $id_user)
				{
					$this->session->set_flashdata('msj_error', 'No puedes activar el Usuario con el que estas logueado !!!');
					redirect(base_url('user'));
				}
				else
				{
					$data = array(
						'user' => $this->User_model->get_user($id_user),
					);
					if (isset($data['user']))
					{
						$data = array(
							'state' => '1',
						);
						$this->User_model->enabled($id_user, $data);
						$this->session->set_flashdata('msj_success', 'El Usuario se activo correctamente !!!');
						redirect(base_url('user'));
					}
					else
					{
						$this->session->set_flashdata('msj_error', 'El Usuario no existe !!!');
						redirect(base_url('user'));
					}
				}
			}
		}
		elseif ($this->session->userdata('id_profile') === '3') // usuario.
		{
			$this->session->set_flashdata('msj_error', 'No tiene privilegios activar a un Usuario !!!');
			redirect(base_url('dashboard'));
		}
		else //Cualquier otro perfil fuera de los tres principales.
		{
			redirect(base_url('login/login_form'));
		}
	}

	public function delete($id_user, $id_profile, $username)
	{
		if ($this->session->userdata('id_profile') === '1') // system.
		{
			if ($id_profile === '1')
			{
				$this->session->set_flashdata('msj_error', 'No puedes eliminar a Usuarios predeterminados del sistema !!!');
				redirect(base_url('dashboard'));
			}
			else
			{
				if ($this->session->userdata('id_user') === $id_user)
				{
					$this->session->set_flashdata('msj_error', 'No puedes eliminar el Usuario con el que estas logueado !!!');
					redirect(base_url('user'));
				}
				else
				{
					$data = array(
						'user' => $this->User_model->get_user($id_user),
					);
					if (isset($data['user']))
					{
						//Eliminar directorio y contenidos recursivamente
						$ruta = './assets/profile/' . $username;
						if (is_dir($ruta))
						{
        					//Obtener un arreglo con directorios y archivos
							$subdirectorios_o_archivos = scandir($ruta);
							foreach ($subdirectorios_o_archivos as $subdirectorio_o_archivo)
							{
            				//Omitir . y .., pues son directorios que se refieren al directorio actual o al directorio padre
								if ($subdirectorio_o_archivo != "." && $subdirectorio_o_archivo != "..")
								{
                					# Si es un directorio, recursión
									if (is_dir($ruta . "/" . $subdirectorio_o_archivo))
									{
										eliminar_directorio_recursivamente($ruta . "/" . $subdirectorio_o_archivo);
									} else {
                    					# Si es un archivo, lo eliminamos con unlink
										unlink($ruta . "/" . $subdirectorio_o_archivo);
									}
								}
							}
        					# Al final de todo, el directorio estará vacío
        					# y podremos usar rmdir
							rmdir($ruta);
						}
						$this->User_model->delete($id_user, $username);
						$this->session->set_flashdata('msj_success', 'El Usuario se elimino correctamente !!!');
						redirect(base_url('user'));
					}
					else
					{
						$this->session->set_flashdata('msj_error', 'El Usuario no existe !!!');
						redirect(base_url('user'));
					}
				}
			}
		}
		elseif ($this->session->userdata('id_profile') === '2') // admin.
		{
			$this->session->set_flashdata('msj_error', 'No tiene privilegios para eliminar a un Usuario !!!');
			redirect(base_url('dashboard'));
		}
		elseif ($this->session->userdata('id_profile') === '3') // usuario.
		{
			$this->session->set_flashdata('msj_error', 'No tiene privilegios para eliminar a un Usuario !!!');
			redirect(base_url('dashboard'));
		}
		else //Cualquier otro perfil fuera de los tres principales.
		{
			redirect(base_url('login/login_form'));
		}
	}

	public function edit_pass($id_user, $id_profile)
	{
		$this->session->set_flashdata('id_profile_pass', $id_profile);
		if ($this->session->userdata('id_profile') === '1') // system.
		{
			$data = array(
				'user'	  => $this->User_model->get_user($id_user),
				'title'	  => '-- Edit Pass User --',
				'bg_body' => '',
				'header'  => 'layout/header',
				'content' => 'user/edit_pass',
				'footer'  => 'layout/footer',
			);
			$this->load->view('layout/template', $data);
		}
		elseif ($this->session->userdata('id_profile') === '2') // admin.
		{
			if ($id_profile === '1') // Si es un usuario del sistema
			{
				$this->session->set_flashdata('msj_error', 'No puedes cambiar la clave de los Usuarios predeterminados del sistema !!!');
				redirect(base_url('dashboard'));
			}
			else
			{
				$data = array(
					'user'	  => $this->User_model->get_user($id_user),
					'title'	  => '-- Edit Pass User --',
					'bg_body' => '',
					'header'  => 'layout/header',
					'content' => 'user/edit_pass',
					'footer'  => 'layout/footer',
				);
				$this->load->view('layout/template', $data);
			}
		}
		elseif ($this->session->userdata('id_profile') === '3') // usuario.
		{
			if ($this->session->userdata('id_user') === $id_user) // Si el id es igual puede cambiar la clave.
			{
				$data = array(
					'user'	  => $this->User_model->get_user($id_user),
					'title'	  => '-- Edit Pass User --',
					'bg_body' => '',
					'header'  => 'layout/header',
					'content' => 'user/edit_pass',
					'footer'  => 'layout/footer',

				);
				$this->load->view('layout/template', $data);
			}
			else
			{
				$this->session->set_flashdata('msj_error', 'No puedes cambiar la clave de otro Usuario !!!');
				redirect(base_url('dashboard'));
			}
		}
		else // Si no existe ninguna session reenvia a la vista login.
		{
			redirect(base_url('login/login_form'));
		}
	}

	public function update_pass()
	{
		$id_user  = $this->input->post('id_user');
		$password = $this->input->post('password');

		if ($this->session->userdata('id_profile') === '1') // system.
		{
			// Validaciones del formulario con la librería form_validation
			$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]|max_length[12]|strip_tags|xss_clean',
				array(
					'required'	 => 'Este campo es requerido',
					'min_length' => 'Minimo 6 Caracteres',
					'max_length' => 'Maximo 12 Caracteres',
				)
			);
			$this->form_validation->set_rules('r_password', 'Password', 'trim|required|matches[password]|strip_tags|xss_clean',
				array(
					'required' => 'Debe repetir la contraseña',
					'matches'  => 'Las contraseñas no coinciden',
				)
			);
			if ($this->form_validation->run() == FALSE)
			{
				$this->edit_pass($id_user, $id_profile = $this->session->flashdata('id_profile_pass'));
			}
			else
			{
				$data = array(
					'id_user'	   => $id_user,
					'password'	   => sha1($password),
					'updated_pass' => date('Y-m-d H:i:s'),
				);
				if ($this->User_model->update_pass($id_user, $data))
				{
					$this->session->set_flashdata('msj_success', 'El Usuario se actualizó correctamente !!!');
					redirect(base_url('user'));
				}
				else
				{
					$this->session->set_flashdata('msj_error', 'No se actualizó el Usuario !!!');
					redirect(base_url('user'));
				}
			}
		}
		elseif ($this->session->userdata('id_profile') === '2') // admin.
		{
			// Validaciones del formulario con la librería form_validation
			$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]|max_length[12]|strip_tags|xss_clean',
				array(
					'required'   => 'Este campo es requerido.',
					'min_length' => 'Minimo 6 Caracteres.',
					'max_length' => 'Maximo 12 Caracteres',
				)
			);
			$this->form_validation->set_rules('r_password', 'Password', 'trim|required|matches[password]|strip_tags|xss_clean',
				array(
					'required' => 'Este campo es requerido.',
					'matches'  => 'Las contraseñas no coinciden.',
				)
			);
			if ($this->form_validation->run() == FALSE)
			{
				$this->edit_pass($id_user, $id_profile = $this->session->flashdata('id_profile_pass'));
			}
			else
			{
				$data = array(
					'id_user'	   => $id_user,
					'password'	   => sha1($password),
					'updated_pass' => date('Y-m-d H:i:s'),
				);
				if ($this->User_model->update_pass($id_user, $data))
				{
					$this->session->set_flashdata('msj_success', 'La Contraseña se actualizo correctamente !!!');
					redirect(base_url('user'));
				}
				else
				{
					$this->session->set_flashdata('msj_error', 'No se actualizó la Contraseña !!!');
					redirect(base_url('user'));
				}
			}
		}
		elseif ($this->session->userdata('id_profile') === '3') // usuario.
		{
			// Validaciones del formulario con la librería form_validation
			$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]|max_length[12]|strip_tags|xss_clean',
				array(
					'required'   => 'Este campo es requerido.',
					'min_length' => 'Minimo 6 Caracteres.',
					'max_length' => 'Maximo 12 Caracteres',
				)
			);
			$this->form_validation->set_rules('r_password', 'Password', 'trim|required|matches[password]|strip_tags|xss_clean',
				array(
					'required' => 'Este campo es requerido.',
					'matches'  => 'Las contraseñas no coinciden.',
				)
			);
			if ($this->form_validation->run() == FALSE)
			{
				$this->edit_pass($id_user, $id_profile = $this->session->flashdata('id_profile_pass'));
			}
			else
			{
				$data = array(
					'id_user'	   => $id_user,
					'password'	   => sha1($password),
					'updated_pass' => date('Y-m-d H:i:s'),
				);
				if ($this->User_model->update_pass($id_user, $data))
				{
					$this->session->set_flashdata('msj_success', 'La Contraseña se actualizo correctamente !!!');
					redirect(base_url('user'));
				}
				else
				{
					$this->session->set_flashdata('msj_error', 'No se actualizó la Contraseña !!!');
					redirect(base_url('user'));
				}
			}
		}
		else //Cualquier otro perfil fuera de los tres principales.
		{
			redirect(base_url('login/login_form'));
		}
	}

	public function do_upload() // Función para subir la imagen al servidor.
	{
		// Capturamos algunos datos necesarios del usuario.
		$id_user    = $this->input->post('id_user');
		$username   = $this->input->post('username');
		// Configuración de la imagen: como la ruta y el tamaño, al nombre_archivo asigno el valor de $ id_user para que sobrescriba si un usuario carga más imágenes de perfil.
		$config['upload_path']      = './assets/profile/'.$username.'/';
		$config['allowed_types']    = 'gif|jpg|png';
		$config['overwrite']        = true;
		$config['remove_spaces']    = true;
		$config['file_name']        = $username;
		$config['max_size']         = '2000';
		$config['max_width']        = '2048';
		$config['max_height']       = '2048';
		$this->load->library('upload', $config);
		// Si no se selecciona una imagen o si la imagen no cumple con los estándares establecidos.
		if (!$this->upload->do_upload())
		{
			$this->session->set_flashdata('msj_error', 'Select an image .gif, .jpg, .png- Max: 2048 x 2048 y 2Mb !!!');
			$this->edit($id_user, $id_profile = $this->session->flashdata('id_profile_edit'));
		}
		else
		{
			// Si la validación se cumple, subimos la imagen. Los datos se envían al modelo a insertar.
			$file_info = $this->upload->data();
			$data = array('upload_data' => $this->upload->data());
			$imagen = $file_info['file_name'];
			$subir = $this->User_model->subir($imagen,$id_user);
			$this->session->set_flashdata('msj-success', 'Subida');
			redirect('user/profile/'.$id_user);
		}
	}

}

/* End of file User.php */
/* Location: ./application/controllers/User.php */
