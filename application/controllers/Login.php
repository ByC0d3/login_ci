<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('User_model');
	}

	public function index()
	{
		if ($this->session->userdata('id_profile') === '1'
			or $this->session->userdata('id_profile') === '2'
			or $this->session->userdata('id_profile') === '3')
		{
			redirect('dashboard');
		}
		else // Si no existe ninguna de las sessiones anteriores (1, 2 o 3) se redirige a la vista login.
		{
			$this->session->sess_destroy();
			redirect(base_url('login/login_form'));
		}
	}

	public function login_form(){
		$data = array(
			'token' 	=> $this->token(),
			'title' 	=> '--- Login Codeigniter ---', // Envia el titulo a la vista dashboard.
			'header' 	=> 'layout/header_login',
			'content'	=> 'login/login',
			'footer' 	=> 'layout/footer_login',
		);
		$this->load->view('layout/template_login', $data);
	}

	public function token()
	{
		$token = sha1(uniqid(rand(),true));
		$this->session->set_userdata('token',$token);
		return $token;
	}

	public function login()
	{
		if($this->input->post('token') && $this->input->post('token') === $this->session->userdata('token'))
		{
			// Validaciones del formulario con la librería form_validation
			$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[3]|max_length[12]|strip_tags|xss_clean|alpha',
				array(
					'required'	 	=> 'Este campo es requerido.',
					'min_length'	=> 'Minimo 3 Caracteres.',
					'max_length'	=> 'Maximo 12 Caracteres',
					'alpha'			=> 'Introduzca solo letras.',
				));
			$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]|max_length[12]|strip_tags|xss_clean',
				array(
					'required'		=> 'Este campo es requerido.',
					'min_length'	=> 'Minimo 6 Caracteres.',
					'max_length'	=> 'Maximo 12 Caracteres',
				)
			);
			if ($this->form_validation->run() == FALSE) // Si no se cumple la validacion carga el formulario login nuevamente con los errores.
			{
				$this->login_form();
			}
			else
			{
				$username 		= $this->input->post('username');
				$password 		= sha1($this->input->post('password'));
				$check_user 	= $this->User_model->login_user($username,$password);
				if($check_user == TRUE)
				{
					$data = array(
						'is_logued_in'	=>	TRUE,
						'id_user'		=>	$check_user->id_user,
						'id_profile'	=>	$check_user->id_profile,
						'username' 		=> 	$check_user->username,
						'name' 			=> 	$check_user->name,
						'lastname' 		=> 	$check_user->lastname,
						'photo' 		=> 	$check_user->photo,
					);
					$this->session->set_userdata($data);
					$this->session->set_flashdata('msj_success', 'Bienvenido '.$this->session->userdata('username').' !!!');
					$this->index();
				}
				else // Si el usuario o la contraseña son incorrectos se reenvia a la vista login.
				{
					$this->session->set_flashdata('msj_error','Datos incorrectos o Usuario desactivado, de no poder entrar comuníquese con un Administrador !!!');
					redirect(base_url('login/login_form'));
				}
			}
		}
		else
		{
			redirect(base_url('login/login_form'));
		}
	}

	public function logout()
	{
		$this->session->sess_destroy(); // Cierra y destruye la session y redirige a la vista login.
		$this->index();
	}

}

/* End of file Login.php */
/* Location: ./application/controllers/Login.php */
