<!DOCTYPE html>
<html>
<head>
	<!-- charset utf-8 -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Carga el título que se envía desde cada función del controlador -->
	<title><?=$title?></title>
	<!-- Favicon -->
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('assets/img/logo.png'); ?>">
	<!-- Bootstrap 4.1.1 -->
	<link rel="stylesheet" href="<?php echo base_url('assets/bootstrap-4.1.1/css/bootstrap.min.css');?>">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="<?php echo base_url('assets/fontawesome-5.3/css/all.min.css'); ?>">
	<!-- Estilos propios -->
	<link rel="stylesheet" href="<?php echo base_url('assets/css/styles.css'); ?>">
</head>
<body class="bg-login">
	<?php $this->load->view($header);?>
	<?php $this->load->view($content);?>
	<?php $this->load->view($footer);?>
	<!-- Jquery 3.3.1 -->
	<script src="<?php echo base_url('assets/js/jquery-3.3.1.min.js'); ?>"></script>
	<!-- jquery Validate -->
	<script src="<?php echo base_url('assets/js/jquery.validate.min.js'); ?>"></script>
	<!-- additional-methods -->
	<script src="<?php echo base_url('assets/js/additional-methods.min.js'); ?>"></script>
	<!-- Bootstrap 4.1.1 -->
	<script src="<?php echo base_url('assets/bootstrap-4.1.1/js/bootstrap.min.js'); ?>"></script>
	<!-- Script propios -->
	<script src="<?php echo base_url('assets/js/scripts.js'); ?>"></script>
</body>
</html>
