<!DOCTYPE html>
<html lang="es">
<head>
	<!-- charset utf-8 -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Carga el título que se envía desde cada función del controlador -->
	<title><?=$title?></title>
	<!-- Favicon -->
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('assets/img/logo.png'); ?>">
	<!-- Bootstrap 4.1.1 -->
	<link rel="stylesheet" href="<?php echo base_url('assets/bootstrap-4.1.1/css/bootstrap.min.css');?>">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="<?php echo base_url('assets/fontawesome-5.3/css/all.min.css'); ?>">
	<!-- DataTables -->
	<link rel="stylesheet" href="<?php echo base_url('assets/datatables/css/dataTables.bootstrap4.min.css'); ?>">
	<!-- Estilos propios -->
	<link rel="stylesheet" href="<?php echo base_url('assets/css/styles.css'); ?>">

</head>
<body class="<?=$bg_body?>">
	<?php $this->load->view($header);?>
	<?php $this->load->view($content);?>
	<?php $this->load->view($footer);?>
	<!-- Jquery 3.3.1 -->
	<script src="<?php echo base_url('assets/js/jquery-3.3.1.min.js'); ?>"></script>
	<!-- jquery Validate -->
	<script src="<?php echo base_url('assets/js/jquery.validate.min.js'); ?>"></script>
	<!-- additional-methods -->
	<script src="<?php echo base_url('assets/js/additional-methods.min.js'); ?>"></script>
	<!-- additional-methods -->
	<script src="<?php echo base_url('assets/js/bootstrap-filestyle.min.js'); ?>"></script>
	<!-- Popper -->
	<script src="<?php echo base_url('assets/js/popper.min.js'); ?>"></script>
	<!-- Bootstrap 4.1.1 -->
	<script src="<?php echo base_url('assets/bootstrap-4.1.1/js/bootstrap.min.js'); ?>"></script>
	<!-- InputMask -->
	<script src="<?php echo base_url('assets/js/jquery.mask.min.js'); ?>"></script>
	<!-- DataTables -->
	<script src="<?php echo base_url('assets/datatables/js/jquery.dataTables.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/datatables/js/dataTables.bootstrap4.min.js'); ?>"></script>
	<!-- Script propios -->
	<script src="<?php echo base_url('assets/js/scripts.js'); ?>"></script>
</body>
</html>
