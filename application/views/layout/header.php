<!-- Logo y Dashboard -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
	<a class="navbar-brand" href="<?php echo base_url('dashboard') ?>">
		<img src="<?php echo base_url('assets/img/logo.png'); ?>" width="30" height="30" class="d-inline-block align-top" alt=""> Dashboard
	</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<div class="navbar-nav mr-auto">
			<?php
			if ($this->session->userdata('id_profile') === '1'){ ?> <!-- Vista para system -->
				<li class="nav-item active">
					<a class="nav-link" href="<?php echo base_url('user') ?>">Usuarios</a>
				</li>
				<li class="nav-item active">
					<a class="nav-link" href="#">Link 1</a>
				</li>
				<li class="nav-item active">
					<a class="nav-link" href="#">Link 2</a>
				</li>
				<li class="nav-item active">
					<a class="nav-link" href="#">Link 3</a>
				</li>
				<li class="nav-item active">
					<a class="nav-link" href="#">Link 4</a>
				</li>
			<?php }
			elseif ($this->session->userdata('id_profile') === '2'){ ?> <!-- Vista para admin -->
				<li class="nav-item active">
					<a class="nav-link" href="<?php echo base_url('user') ?>">Usuarios</a>
				</li>
				<li class="nav-item active">
					<a class="nav-link" href="#">Link 1</a>
				</li>
				<li class="nav-item active">
					<a class="nav-link" href="#">Link 2</a>
				</li>
				<li class="nav-item active">
					<a class="nav-link" href="#">Link 3</a>
				</li>
			<?php }	elseif ($this->session->userdata('id_profile') === '3'){ ?> <!-- Vista para usuario -->
				<li class="nav-item active">
					<a class="nav-link" href="<?php echo base_url('user') ?>">Usuarios</a>
				</li>
				<li class="nav-item active">
					<a class="nav-link" href="#">Link 1</a>
				</li>
				<li class="nav-item active">
					<a class="nav-link" href="#">Link 2</a>
				</li>
			<?php } ?>
		</div>
		<div class="navbar-nav dropdown-menu-right">
			<li class="nav-item prueba">
				<a href="<?php echo base_url('user/profile/'.$this->session->userdata('id_user'));?>"><img alt="Sin Imagen" src="<?=base_url()?>assets/profile/<?=$this->session->userdata('username').'/'.$this->session->userdata('photo')?>" class="rounded-circle image-nav"></a>
			</li>
			<li class="nav-item dropdown">
				<a class="nav-link active dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<?=$this->session->userdata('username')?>
				</a>
				<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
					<a class="dropdown-item" href="<?php echo base_url('user/profile/'.$this->session->userdata('id_user'));?>"><i class="fas fa-user-cog text-dark"></i> Perfil</a>
					<div class="dropdown-divider"></div>
					<a class="dropdown-item" href="<?php echo base_url('login/logout')?>"><i class="fas fa-sign-out-alt text-dark"></i> Salir</a>
				</div>
			</li>
		</div>
	</div>
</nav>
<div class="container brea">
<!-- breadcrumb -->
	<ol class="breadcrumb">
		<li class="active">
			<i class="fas fa-user text-dark"></i> Bienvenido <strong> <i class="text-capitalize"><?=$this->session->userdata('name')?> <?=$this->session->userdata('lastname')?></strong></i>
		</li>
	</ol>
</div>
