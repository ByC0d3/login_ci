<?php $user_count = $this->db->count_all('users'); ?> <!-- Cuenta la cantidad de registros de la tabla users. -->
<div class="container">
	<div class="jumbotron">
		<h1 class="display-4">Hola, Mundo !!!</h1>
		<p class="lead">Login en Codeigniter y Bootstrap.</p>
		<p class="lead">
			<button type="button" class="btn btn-dark" data-toggle="modal" data-target=".bd-example-modal-lg">Caracteristicas</button>
		</p>
		<!-- modal -->
		<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLongTitle">Login Codeigniter y Bootstrap</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body text-justify">
						<ul class="list-unstyled">
							<li><strong>Fácil, rápido y seguro.</strong></li>
							<ul>
								<li>Fácil de manejar y entender.</li>
								<li>Lijero a la hora de cargar..</li>
								<li>Seguridad, manejo de token en login, cifrado y sesiones para mantener seguro al usuario.</li>
							</ul>
							<li><strong>Adaptable y escalable..</strong></li>
							<ul>
								<li>Adaptable, fácil de incluir en cualquier proyecto.</li>
								<li>Escalable, puede ser mejorado e incluir nuevas funciones.</li>
							</ul>
							<li><strong>El Código.</strong></li>
							<ul>
								<li>Código limpio, ordenado y comentado.</li>
								<li>La libreria propia js esta ordenada y comentada, se puede entender y modificar fácilmente.</li>
								<li>Así mismo la librería css, contiene algunos estilos propios para mejorar la apariencia de la interfaz.</li>
								<li>Se agrego la estructura de la base de datos en un .sql.</li>
								<li>La misma posee tres registros de prueba uno de ellos es el system, no puede ser modificado ni eliminado a travez de la aplicación, es un usuario propio del sistema.</li>
							</ul>
							<li><strong>Internamente.</strong></li>
							<ul>
								<li>Las funciones se dividen según el perfil del usuario.</li>
								<li>Los iconos y enlaces de las vistas se muestran según el perfil del usuario..</li>
								<li>Las contraseñas están cifradas en sha1.</li>
							</ul>
						</ul>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary btn-dark" data-dismiss="modal">Cerrar</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php if ($this->session->userdata('id_profile') === '1'){ ?> <!-- Vista para System. -->
	<div class="row">
		<div class="col-md-3 col-sm-6">
			<div class="card text-white bg-info">
				<div class="card-header bg-info">Total User<a href="<?php echo base_url('user') ?>" class="badge badge-light icon-header-cars text-dark" data-toggle="tooltip" data-placement="top" title="Total User"><?php echo $user_count; ?></a></div>
				<div class="card-body">
					<!--<h5 class="card-title">title</h5>-->
					<i class="fas fa-users icon-cards"></i><p class="card-text text-right">Users.</p>
				</div>
				<div class="card-footer bg-info">
					<a href="<?php echo base_url('user/add')?>" class="btn btn-sm btn-light">Create</a>
				</div>
			</div>
		</div>
		<div class="col-md-3 col-sm-6">
			<div class="card text-white bg-warning">
				<div class="card-header bg-warning">Total Card<a href="#" class="badge badge-light icon-header-cars" data-toggle="tooltip" data-placement="top" title="Inventario">105</a></div>
				<div class="card-body">
					<!--<h5 class="card-title">title</h5>-->
					<i class="fas fa-archive icon-cards"></i><p class="card-text text-right">Items.</p>
				</div>
				<div class="card-footer bg-warning">
					<a href="<?php echo base_url('#')?>" class="btn btn-sm btn-light">Create</a>
				</div>
			</div>
		</div>
		<div class="col-md-3 col-sm-6">
			<div class="card text-white bg-danger">
				<div class="card-header bg-danger">Total Card<a href="#" class="badge badge-light icon-header-cars" data-toggle="tooltip" data-placement="top" title="Compras realizadas">253</a></div>
				<div class="card-body">
					<!--<h5 class="card-title">title</h5>-->
					<i class="fas fa-store icon-cards"></i><p class="card-text text-right">Items.</p>
				</div>
				<div class="card-footer bg-danger">
					<a href="<?php echo base_url('#')?>" class="btn btn-sm btn-light">Create</a>
				</div>
			</div>
		</div>
		<div class="col-md-3 col-sm-6">
			<div class="card text-white bg-dark">
				<div class="card-header bg-dark">Total Card<a href="#" class="badge badge-light icon-header-cars" data-toggle="tooltip" data-placement="top" title="Total de Ventas">378</a></div>
				<div class="card-body">
					<!--<h5 class="card-title">title</h5>-->
					<i class="fas fa-credit-card icon-cards"></i><p class="card-text text-right">Items.</p>
				</div>
				<div class="card-footer bg-dark">
					<a href="<?php echo base_url('#')?>" class="btn btn-sm btn-light">Create</a>
				</div>
			</div>
		</div>
	</div>
	<?php }	elseif ($this->session->userdata('id_profile') === '2'){ ?> <!-- Vista para Admin. -->
	<div class="row">
		<div class="col-md-3">
			<div class="card text-white bg-info">
				<div class="card-header bg-info">Total User<a href="<?php echo base_url('user') ?>" class="badge badge-light icon-header-cars text-dark" data-toggle="tooltip" data-placement="top" title="Total User"><?php echo $user_count; ?></a></div>
				<div class="card-body">
					<!--<h5 class="card-title">title</h5>-->
					<i class="fas fa-users icon-cards"></i><p class="card-text text-right">Users.</p>
				</div>
				<div class="card-footer bg-info">
					<a href="<?php echo base_url('user/add')?>" class="btn btn-sm btn-light">Create</a>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="card text-white bg-warning">
				<div class="card-header bg-warning">Card<a href="#" class="badge badge-light icon-header-cars" data-toggle="tooltip" data-placement="top" title="Inventario">105</a></div>
				<div class="card-body">
					<!--<h5 class="card-title">title</h5>-->
					<i class="fas fa-archive icon-cards"></i><p class="card-text text-right">Items.</p>
				</div>
				<div class="card-footer bg-warning">
					<a href="<?php echo base_url('#')?>" class="btn btn-sm btn-light">Create</a>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="card text-white bg-danger">
				<div class="card-header bg-danger">Card<a href="#" class="badge badge-light icon-header-cars" data-toggle="tooltip" data-placement="top" title="Compras realizadas">253</a></div>
				<div class="card-body">
					<!--<h5 class="card-title">title</h5>-->
					<i class="fas fa-store icon-cards"></i><p class="card-text text-right">Items.</p>
				</div>
				<div class="card-footer bg-danger">
					<a href="<?php echo base_url('#')?>" class="btn btn-sm btn-light">Create</a>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="card text-white bg-dark">
				<div class="card-header bg-dark">Card<a href="#" class="badge badge-light icon-header-cars" data-toggle="tooltip" data-placement="top" title="Total de Ventas">378</a></div>
				<div class="card-body">
					<!--<h5 class="card-title">title</h5>-->
					<i class="fas fa-credit-card icon-cards"></i><p class="card-text text-right">Items.</p>
				</div>
				<div class="card-footer bg-dark">
					<a href="<?php echo base_url('#')?>" class="btn btn-sm btn-light">Create</a>
				</div>
			</div>
		</div>
	</div>
	<?php }	elseif ($this->session->userdata('id_profile') === '3'){ ?> <!-- Vista para Usuarios. -->
	<div class="row">
		<div class="col-md-3">
			<div class="card text-white bg-info">
				<div class="card-header bg-info">Total User<a href="<?php echo base_url('user') ?>" class="badge badge-light icon-header-cars text-dark" data-toggle="tooltip" data-placement="top" title="Total User"><?php echo $user_count; ?></a></div>
				<div class="card-body">
					<!--<h5 class="card-title">title</h5>-->
					<i class="fas fa-users icon-cards"></i><p class="card-text text-right">Registered users.</p>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="card text-white bg-warning">
				<div class="card-header bg-warning">Inventory<a href="#" class="badge badge-light icon-header-cars" data-toggle="tooltip" data-placement="top" title="Inventario">105</a></div>
				<div class="card-body">
					<!--<h5 class="card-title">title</h5>-->
					<i class="fas fa-archive icon-cards"></i><p class="card-text text-right">Items registered.</p>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="card text-white bg-danger">
				<div class="card-header bg-danger">Purchases<a href="#" class="badge badge-light icon-header-cars" data-toggle="tooltip" data-placement="top" title="Compras realizadas">253</a></div>
				<div class="card-body">
					<!--<h5 class="card-title">title</h5>-->
					<i class="fas fa-store icon-cards"></i><p class="card-text text-right">Purchases.</p>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="card text-white bg-dark">
				<div class="card-header bg-dark">Sales<a href="#" class="badge badge-light icon-header-cars" data-toggle="tooltip" data-placement="top" title="Total de Ventas">378</a></div>
				<div class="card-body">
					<!--<h5 class="card-title">title</h5>-->
					<i class="fas fa-credit-card icon-cards"></i><p class="card-text text-right">Sales of the day.</p>
				</div>
			</div>
		</div>
	</div>
	<?php }	else { ?> <!-- Cualquier otro perfil fuera de los tres principales. -->
		<?php redirect(base_url('login/login_form')); ?>
	<?php } ?>
</div>

<div class="container">
	<!-- Imprime un msj de bienvenida en caso de accso correcto. -->
	<?php if ($this->session->flashdata("msj_success")):?>
		<div class="row justify-content-center" >
			<div class="alert alert-success text-center content alert-container" role="alert">
				<i class="fas fa-thumbs-up"></i> <strong><?php echo $this->session->flashdata('msj_success'); ?></strong>
			</div>
		</div>
	<?php endif; ?>
</div>

<div class="container">
	<!-- Imprime un msj de error en caso de datos incorrectos. -->
	<?php if ($this->session->flashdata("msj_error")):?>
		<div class="row justify-content-center" >
			<div class="alert alert-danger text-center content alert-container" role="alert">
				<i class="fas fa-exclamation-triangle"></i> <strong><?php echo $this->session->flashdata('msj_error'); ?></strong>
			</div>
		</div>
	<?php endif; ?>
</div>