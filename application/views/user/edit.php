<div class="container">
	<div class="row justify-content-center">
		<h1 class="display-4">Editar Usuario</h1>
	</div>
	<div class="row justify-content-center mt-3">
		<div class="col-md-8">
			<div class="form-row">
				<div class="form-group col-md-12 text-center">
					<img alt="Sin Imagen" src="<?=base_url()?>assets/img/profile/<?=$user->photo?>" class="rounded-circle image-add" data-toggle="tooltip" data-placement="top" title="prueba">
				</div>
			</div>
			<div class="form-row">
				<div class="form-group col-md-12 text-center">
					<form action="<?=base_url("user/do_upload")?>" id="form-img" method="post" class="" enctype="multipart/form-data">
						<input type="hidden" name="id_user" value="<?php echo $user->id_user;?>">
						<input type="hidden" name="username" value="<?php echo $user->username;?>">
						<label>Imagen de Perfil:</label>
						<input type="file" class="form-control-file" id="file-picker" name="userfile" >
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-md-12 text-center">
						<button type="submit" id="file-picker" class="btn btn-danger btn-form"><i class="fas fa-upload"></i> Subir</button>
					</form>
				</div>
			</div>
			<form action="<?php echo base_url('user/update'); ?>" method="post" id="form-edit-user" class="form-horizontal" autocomplete="off">
				<input type="hidden" name="id_user" value="<?php echo $user->id_user;?>">
				<div class="form-row">
					<div class="form-group col-md-6">
						<label for="id_profile" class="control-label">Perfil:</label>
						<?php if ($this->session->userdata('id_profile') === '1'){ ?> <!-- Vista para system. -->
						<select name="id_profile" class="form-control">
							<?php foreach($get_profiles as $value):?>
								<option value="<?php echo $value->id_profile; ?>" <?php echo $value->id_profile == $user->id_profile ? "selected":"";?>><?php echo $value->profile; ?></option>
							<?php endforeach;?>
						</select>
						<?php }	elseif ($this->session->userdata('id_profile') === '2'){ ?> <!-- Vista para admin. -->
						<select name="id_profile" class="form-control">
							<?php foreach($get_profiles_admin as $value):?>
								<option value="<?php echo $value->id_profile; ?>" <?php echo $value->id_profile == $user->id_profile ? "selected":"";?>><?php echo $value->profile; ?></option>
							<?php endforeach;?>
						</select>
						<?php }	elseif ($this->session->userdata('id_profile') === '3'){ ?> <!-- Vista para usuario. -->
						<select name="id_profile" id="id_profile" class="form-control" readonly>
							<option value="3" >user</option>
						</select>
						<span class="error-form-validation"><?php } echo form_error('id_profile'); ?></span>
					</div>
					<div class="form-group col-md-6">
						<label for="username">Nombre de Usuario:</label>
						<input type="text" name="username" id="username" class="form-control text-primary" value="<?php echo $user->username; ?>" maxlength="12" placeholder="Nombre de Usuario">
						<span class="error-form-validation"><?php echo form_error('username'); ?></span>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-md-6">
						<label for="name">Nombre:</label>
						<input type="text" name="name" id="name" class="form-control" value="<?php echo $user->name; ?>" maxlength="12" placeholder="Nombre">
						<span class="error-form-validation"><?php echo form_error('name'); ?></span>
					</div>
					<div class="form-group col-md-6">
						<label for="lastname">Apellido:</label>
						<input type="text" name="lastname" id="lastname" class="form-control" value="<?php echo $user->lastname; ?>" maxlength="12" placeholder="Apellido">
						<span class="error-form-validation"><?php echo form_error('lastname'); ?></span>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-md-6">
						<label for="email">Correo:</label>
						<input type="email" name="email" id="email" class="form-control" value="<?php echo $user->email; ?>" maxlength="30" placeholder="Correo">
						<span class="error-form-validation"><?php echo form_error('email'); ?></span>
					</div>
					<div class="form-group col-md-2">
						<label for="id_code" class="control-label">Codigo:</label>
						<select name="id_code" class="form-control">
							<?php foreach($get_codes as $value):?>
								<option value="<?php echo $value->id_code; ?>" <?php echo $value->id_code == $user->id_code ? "selected":"";?>><?php echo $value->code; ?></option>
							<?php endforeach;?>
						</select>
						<span class="error-form-validation"><?php echo form_error('id_code'); ?></span>
					</div>
					<div class="form-group col-md-4">
						<label for="email">teléfono:</label>
						<input type="text" name="phone" id="phone" class="form-control" value="<?php echo $user->phone; ?>" data-mask="0000000"  placeholder="Telefono">
						<span class="error-form-validation"><?php echo form_error('phone'); ?></span>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-md-12">
						<label for="address">Direccion:</label>
						<textarea name="address" class="form-control text-justify" maxlength="150" placeholder="Dirección"><?php echo $user->address; ?></textarea>
						<span class="error-form-validation"><?php echo form_error('address'); ?></span>
					</div>
				</div>
				<a class="btn btn-dark btn-form" href="<?php echo base_url('user/profile/'.$user->id_user)?>"><i class="fas fa-undo"></i> Volver</a>
				<button type="submit" class="btn btn-danger btn-form"><i class="fa fa-user-edit"></i> Editar</button>
			</form>
		</div>
	</div>
</div>

<div class="container">
	<!-- Imprime un msj success despues de registrar un usuario. -->
	<?php if ($this->session->flashdata("msj_success")):?>
		<div class="row justify-content-center" >
			<div class="alert alert-success text-center content alert-container" role="alert">
				<i class="fas fa-thumbs-up"></i> <strong><?php echo $this->session->flashdata('msj_success'); ?></strong>
			</div>
		</div>
	<?php endif; ?>
</div>

<div class="container">
	<!-- Imprime un msj de de error en caso de datos incorrectos. -->
	<?php if ($this->session->flashdata("msj_error")):?>
		<div class="row justify-content-center" >
			<div class="alert alert-danger text-center content alert-container" role="alert">
				<i class="fas fa-exclamation-triangle"></i> <strong><?php echo $this->session->flashdata('msj_error'); ?></strong>
			</div>
		</div>
	<?php endif; ?>
</div>
