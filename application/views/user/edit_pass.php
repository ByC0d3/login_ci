<div class="container">
	<div class="row justify-content-center">
		<h1 class="display-4">Editar Contraseña de Usuario</h1>
	</div>
	<div class="row justify-content-center">
		<h3>
			Nombre de Usuario:
			<small class="text-danger"><?php echo $user->username; ?></small>
		</h3>
	</div>
	<div class="row justify-content-center mt-3">
		<div class="col-md-8">
			<form action="<?php echo base_url('user/update_pass'); ?>" method="post" id="form-edit-pass" class="form-horizontal" autocomplete="off">
				<input type="hidden" name="id_user" value="<?php echo $user->id_user;?>" readonly="readonly">
				<div class="form-row">
					<div class="form-group col-md-6">
						<label for="password">Nueva Contraseña:</label>
						<input type="password" class="form-control" name="password" id="password" value="" maxlength="12" placeholder="Nueva Contraseña">
						<span class="error-form-validation"><?php echo form_error('password'); ?></span>
					</div>
					<div class="form-group col-md-6">
						<label for="r_password">Repita la Nueva Contraseña:</label>
						<input type="password" class="form-control" name="r_password" id="r_password" value="" maxlength="12" placeholder="Repita la Nueva Contraseña">
						<span class="error-form-validation"><?php echo form_error('r_password'); ?></span>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-md-2">
						<label></label>
						<a class="btn btn-dark btn-block role="button" href="<?php echo base_url('user')?>">Volver</a>
					</div>
					<div class="form-group col-md-2">
						<label></label>
						<button type="submit" id="btn" class="btn btn-danger btn-block">Enviar</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
