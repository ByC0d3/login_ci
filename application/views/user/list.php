<div class="container">
	<div class="justify-content-center row">
		<h1 class="display-4">Usuarios</h1>
	</div>
	<?php if ($this->session->userdata('id_profile') === '1'){ ?> <!-- Vista para system -->
		<div class="row">
			<a href="<?php echo base_url('user/add'); ?>" class="btn btn-dark btn-form float-right mt-3"><i class="fas fa-user-plus"></i> Agregar</a>
		</div>
	<?php }	elseif ($this->session->userdata('id_profile') === '2'){ ?> <!-- Vista para admin -->
		<div class="row">
			<a href="<?php echo base_url('user/add'); ?>" class="btn btn-dark float-right mt-3"><i class="fa fa-plus"></i> Agregar Usuario</a>
		</div>
	<?php }	elseif ($this->session->userdata('id_profile') === '3'){ ?> <!-- Vista para usuario -->

	<?php } ?>
	<div class="row mt-3">
		<div class="table-responsive">
			<table id="table_list_user" class="table table-sm  table-hover">
				<thead>
					<tr>
						<th>#</th>
						<th>Imagen</th>
						<th>Perfil</th>
						<th>Usuario</th>
						<th>Nombre</th>
						<th>Apellido</th>
						<th>Correo</th>
						<th>Estado</th>
						<th>Acción</th>
					</tr>
				</thead>
				<tbody>
					<?php $num=0; ?>
					<?php if(!empty($users)):?>
						<?php foreach ($users as $user):?>
							<tr>
								<td><?php $num++; echo $num;?></td>
								<td>
									<a href="<?php echo base_url('user/profile/'.$user->id_user);?>"><img alt="Sin Imagen" src="<?=base_url()?>assets/profile/<?=$user->username.'/'.$user->photo?>" class="rounded-circle image-table" data-toggle="tooltip" data-placement="top" title="<?php echo $user->username;?>"></a>
								</td>
								<td><?php echo $user->profile;?></td>
								<td class="text-primary"><a href="<?php echo base_url('user/profile/'.$user->id_user);?>"><?php echo $user->username;?></a></td>
								<td class="text-capitalize"><?php echo $user->name;?></td>
								<td class="text-capitalize"><?php echo $user->lastname;?></td>
								<td><?php echo $user->email;?></td>
								<td><?php echo $user->state;?></td>
								<td>
									<div class="btn-group">
										<a href="<?php echo base_url('user/edit_pass/'.$user->id_user .'/'. $user->id_profile)?>" edit-pass-confirm data-toggle="tooltip" title="Cambiar Clave !!!"><i class="btn fas fa-key icon-table"></i></a>
										<?php if ($this->session->userdata('id_profile') === '1'){ ?> <!-- Vista para system -->
										<?php if ($user->id_profile === '1'): ?>

										<?php elseif ($user->id_profile === '2'): ?>
											<?php if ($user->state === '1'): ?>
											<a href="<?php echo base_url('user/disabled/'.$user->id_user .'/'. $user->id_profile )?>" disabled-user-confirm data-toggle="tooltip" title="Desactivar Usuario !!!"><i class="fas fa-toggle-on icon-table"></i></a>
											<?php else: ?>
												<a href="<?php echo base_url('user/enabled/'.$user->id_user .'/'. $user->state .'/'. $user->id_profile )?>" enabled-user-confirm data-toggle="tooltip" title="Activar Usuario !!!"><i class="fas fa-toggle-off icon-table"></i></a>
											<?php endif ?>
											<a href="<?php echo base_url('user/edit/'.$user->id_user .'/'. $user->id_profile)?>" edit-user-confirm data-toggle="tooltip" title="Editar Usuario !!!" class=""><i class="fas fa-user-edit icon-table"></i></a>
											<a href="<?php echo base_url('user/delete/'.$user->id_user .'/'. $user->id_profile .'/'. $user->username)?>" delete_user_confirm data-toggle="tooltip" title="Eliminar Usuario !!!"><i class="fas fa-user-times icon-table text-danger"></i></a>
										<?php else: ?>
											<?php if ($user->state === '1'): ?>
												<a href="<?php echo base_url('user/disabled/'.$user->id_user .'/'. $user->id_profile )?>" disabled-user-confirm data-toggle="tooltip" title="Desactivar Usuario !!!"><i class="fas fa-toggle-on icon-table"></i></a>
											<?php else: ?>
												<a href="<?php echo base_url('user/enabled/'.$user->id_user .'/'. $user->state .'/'. $user->id_profile )?>" enabled-user-confirm data-toggle="tooltip" title="Activar Usuario !!!"><i class="fas fa-toggle-off icon-table"></i></a>
											<?php endif ?>
											<a href="<?php echo base_url('user/edit/'.$user->id_user .'/'. $user->id_profile)?>" edit-user-confirm data-toggle="tooltip" title="Editar Usuario !!!"><i class="fas fa-user-edit icon-table"></i></a>
											<a href="<?php echo base_url('user/delete/'.$user->id_user .'/'. $user->id_profile .'/'. $user->username )?>" delete_user_confirm data-toggle="tooltip" title="Eliminar Usuario !!!"><i class="fas fa-user-times icon-table text-danger"></i></a>
										<?php endif ?>
										<?php }	elseif ($this->session->userdata('id_profile') === '2'){ ?> <!-- Vista para admin -->
										<?php if ($user->id_profile === '1'): ?>

										<?php elseif ($user->id_profile === '2'): ?>
											<?php if ($user->state === '1'): ?>
												<a href="<?php echo base_url('user/disabled/'.$user->id_user .'/'. $user->id_profile )?>" disabled-user-confirm data-toggle="tooltip" title="Desactivar Usuario !!!"><i class="fas fa-toggle-on icon-table"></i></a>
											<?php else: ?>
												<a href="<?php echo base_url('user/enabled/'.$user->id_user .'/'. $user->state .'/'. $user->id_profile )?>" enabled-user-confirm data-toggle="tooltip" title="Activar Usuario !!!"><i class="fas fa-toggle-off icon-table"></i></a>
											<?php endif ?>
											<a href="<?php echo base_url('user/edit/'.$user->id_user .'/'. $user->id_profile)?>" edit-user-confirm data-toggle="tooltip" title="Editar Usuario !!!"><i class="fas fa-user-edit icon-table"></i></a>
										<?php else: ?>
											<?php if ($user->state === '1'): ?>
												<a href="<?php echo base_url('user/disabled/'.$user->id_user .'/'. $user->id_profile )?>" disabled-user-confirm data-toggle="tooltip" title="Desactivar Usuario !!!"><i class="fas fa-toggle-on icon-table"></i></a>
											<?php else: ?>
												<a href="<?php echo base_url('user/enabled/'.$user->id_user .'/'. $user->state .'/'. $user->id_profile )?>" enabled-user-confirm data-toggle="tooltip" title="Activar Usuario !!!"><i class="fas fa-toggle-off icon-table"></i></a>
											<?php endif ?>
												<a href="<?php echo base_url('user/edit/'.$user->id_user .'/'. $user->id_profile)?>" edit-user-confirm data-toggle="tooltip" title="Editar Usuario !!!"><i class="fas fa-user-edit icon-table"></i></a>
										<?php endif ?>
										<?php }	elseif ($this->session->userdata('id_profile') === '3'){ ?> <!-- Vista para usuario -->
										<?php if ($user->id_profile === '1'): ?>

										<?php elseif ($user->id_profile === '2'): ?>

										<?php else: ?>
											<a href="<?php echo base_url('user/edit/'.$user->id_user .'/'. $user->id_profile)?>" edit-user-confirm data-toggle="tooltip" title="Editar Usuario !!!"><i class="fas fa-user-edit icon-table"></i></a>
										<?php endif ?>
										<?php } ?>
									</div>
								</td>
							</tr>
						<?php endforeach; ?>
					<?php endif;?>
				</tbody>
				<tfoot>
					<!-- tfoot -->
				</tfoot>
			</table>
		</div>
	</div>
</div>

<div class="container">
	<!-- Imprime un msj success de las demas vistas al volver a la lista de usuarios. -->
	<?php if ($this->session->flashdata("msj_success")):?>
		<div class="row justify-content-center" >
			<div class="alert alert-success text-center content alert-container" role="alert">
				<i class="fas fa-thumbs-up"></i> <strong><?php echo $this->session->flashdata('msj_success'); ?></strong>
			</div>
		</div>
	<?php endif; ?>
</div>

<div class="container">
	<!-- Imprime los errorres de las demas vistas al volver a la lista de usuarios. -->
	<?php if ($this->session->flashdata("msj_error")):?>
		<div class="row justify-content-center" >
			<div class="alert alert-danger text-center content alert-container" role="alert">
				<i class="fas fa-exclamation-triangle"></i> <strong><?php echo $this->session->flashdata('msj_error'); ?></strong>
			</div>
		</div>
	<?php endif; ?>
</div>
