<div class="container">
	<div class="row justify-content-center">
		<h1 class="display-4">Perfil de Usuario</h1>
	</div>
	<div class="row justify-content-center mt-3">
		<div class="col-lg-3 text-center">

			<img alt="Sin Imagen" src="<?=base_url()?>assets/profile/<?=$user->username.'/'.$user->photo?>" class="rounded-circle image-profile" data-toggle="tooltip" data-placement="top" title="<?php echo $user->username;?>">
		</div>
		<div class="col-lg-5 ml-4">
			<ul class="nav nav-pills mb-3 mt-3" id="pills-tab" role="tablist">
				<li class="nav-item">
					<a class="nav-link active text-center nav-item-profile" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Perfil</a>
				</li>
				<li class="nav-item">
					<a class="nav-link text-center nav-item-profile" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Bitacora</a>
				</li>
				<li class="nav-item">
					<a class="nav-link text-center nav-item-profile" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">Contacto</a>
				</li>
			</ul>
			<div class="tab-content" id="pills-tabContent">
				<div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
					<dl class="row">
						<dt class="col-sm-5"><em>id User</em></dt>
						<dd class="col-sm-7"><?php echo $user->id_user; ?></dd>
						<dt class="col-sm-5"><em>Usuario:</em></dt>
						<dd class="col-sm-7 text-primary"><?php echo $user->username; ?></dd>
						<dt class="col-sm-5"><em>Nombre:</em></dt>
						<dd class="col-sm-7 text-capitalize"><?php echo $user->name; ?></dd>
						<dt class="col-sm-5"><em>Apellido:</em></dt>
						<dd class="col-sm-7 text-capitalize"><?php echo $user->lastname; ?></dd>
						<dt class="col-sm-5"><em>Estado:</em></dt>
						<dd class="col-sm-7">
							<?php if ($user->state === '1'): ?>
								<i class="fas fa-toggle-on"></i>
							<?php else: ?>
								<i class="fas fa-toggle-off"></i>
							<?php endif ?>
						</dd>
					</dl>
				</div>
				<div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
					<dl class="row">
						<dt class="col-sm-5"><em>Usuario creado:</em></dt>
						<dd class="col-sm-7"><?php echo $user->created_user; ?></dd>
						<dt class="col-sm-5"><em>Usuario modificado:</em></dt>
						<dd class="col-sm-7"><?php echo $user->updated_user; ?></dd>
						<dt class="col-sm-5"><em>Clave modificada:</em></dt>
						<dd class="col-sm-7"><?php echo $user->updated_pass; ?></dd>
					</dl>
				</div>
				<div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
					<div class="row">
						<dt class="col-sm-5"><em>Correo Electrónico:</em></dt>
						<dd class="col-sm-7"><?php echo $user->email; ?></dd>
						<dt class="col-sm-5"><em>Teléfono:</em></dt>
						<dd class="col-sm-7"><?php echo $user->code; ?> - <?php echo $user->phone; ?></dd>
						<dt class="col-sm-5"><em>Dirección:</em></dt>
						<dd class="col-sm-7 text-capitalize text-justify"><?php echo $user->address; ?></dd>
					</div>
				</div>
			</div>
			<a class="btn btn-dark btn-form" href="<?php echo base_url('user')?>"><i class="fas fa-undo"></i> Volver</a>
			<a href="<?php echo base_url('user/edit/'.$user->id_user .'/'. $user->id_profile); ?>" class="btn btn-danger btn-form"><i class="fa fa-user-edit"></i> Editar</a>
		</div>
	</div>
</div>

<div class="container">
	<!-- Imprime un msj de de error en caso de datos incorrectos. -->
	<?php if ($this->session->flashdata("msj-success")):?>
		<div class="row justify-content-center" >
			<div class="alert alert-success text-center content alert-container" role="alert">
				<i class="fas fa-thumbs-up"></i> <strong><?php echo $this->session->flashdata('msj-success'); ?></strong>
			</div>
		</div>
	<?php endif; ?>
</div>

<div class="container">
	<!-- Imprime un msj de de error en caso de datos incorrectos. -->
	<?php if ($this->session->flashdata("msj_error")):?>
		<div class="row justify-content-center" >
			<div class="alert alert-danger text-center content alert-container" role="alert">
				<i class="fas fa-exclamation-triangle"></i> <strong><?php echo $this->session->flashdata('msj_error'); ?></strong>
			</div>
		</div>
	<?php endif; ?>
</div>
