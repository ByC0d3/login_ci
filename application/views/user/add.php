<div class="container">
	<div class="row justify-content-center">
		<h1 class="display-4">Agregar Usuario</h1>
	</div>
	<div class="row justify-content-center mt-3">
		<div class="col-md-8">
			<form action="<?php echo base_url('user/store'); ?>" method="post" id="form-add-user" class="form-horizontal" autocomplete="off">
				<div class="form-row">
					<div class="form-group col-md-6">
						<label for="id_profile">Perfil:</label>
						<?php if ($this->session->userdata('id_profile') === '1'){ ?> <!-- Vista para system. -->
						<select class="form-control" name="id_profile" id="id_profile">
							<option value="" disable selected="selected">Seleccione un Perfil</option>
							<?php foreach($get_profiles as $value):?>
								<option value="<?php echo $value->id_profile;?>"<?php echo set_select('id_profile',$value->id_profile); ?>><?php echo $value->profile?></option>
							<?php endforeach;?>
						</select>
						<?php }	elseif ($this->session->userdata('id_profile') === '2'){ ?> <!-- Vista para admin. -->
						<select class="form-control" name="id_profile" id="id_profile">
							<option value="" disable selected="selected">Seleccione un Perfil</option>
							<?php foreach($get_profiles as $value):?>
								<option value="<?php echo $value->id_profile;?>"<?php echo set_select('id_profile',$value->id_profile); ?>><?php echo $value->profile?></option>
							<?php endforeach; }?>
						</select>
						<span class="error" for="id_profile"></span>
						<span class="error-form-validation"><?php echo form_error('id_profile'); ?></span>
					</div>
					<div class="form-group col-md-6">
						<label for="username">Nombre de Usuario:</label>
						<input type="text" name="username" id="username" class="form-control text-primary" value="<?php echo set_value('username'); ?>" maxlength="12" placeholder="Nombre de Usuario">
						<span class="error-form-validation"><?php echo form_error('username'); ?></span>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-md-6">
						<label for="name">Nombre:</label>
						<input type="text" class="form-control" name="name" id="name" value="<?php echo set_value('name'); ?>" maxlength="12" placeholder="Nombre">
						<span class="error-form-validation"><?php echo form_error('name'); ?></span>
					</div>
					<div class="form-group col-md-6">
						<label for="lastname">Apellido:</label>
						<input type="text" class="form-control" name="lastname" id="lastname" value="<?php echo set_value('lastname'); ?>" maxlength="12" placeholder="Apellido">
						<span class="error-form-validation"><?php echo form_error('lastname'); ?></span>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-md-6">
						<label for="email">Correo:</label>
						<input type="email" class="form-control" name="email" id="email" value="<?php echo set_value('email'); ?>" maxlength="30" placeholder="Correo">
						<span class="error-form-validation"><?php echo form_error('email'); ?></span>
					</div>
					<div class="form-group col-md-2">
						<label for="id_code">Codigo:</label>
						<select class="form-control" name="id_code" id="id_code">
							<option value="" disable selected="selected"> Código</option>
							<?php foreach($get_codes as $value):?>
								<option value="<?php echo $value->id_code;?>"<?php echo set_select('id_code',$value->id_code); ?>><?php echo $value->code?></option>
							<?php endforeach;?>
						</select>
						<span class="error-form-validation"><?php echo form_error('id_code'); ?></span>
					</div>
					<div class="form-group col-md-4">
						<label for="email">teléfono:</label>
						<input type="text" class="form-control" name="phone" data-mask="0000000" value="<?php echo $this->input->post('phone'); ?>" placeholder="Telefono">
						<span class="error-form-validation"><?php echo form_error('phone'); ?></span>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-md-12">
						<label for="address">Direccion:</label>
						<textarea class="form-control" name="address" value="" maxlength="150" placeholder="Dirección"><?php echo set_value('address'); ?></textarea>
						<span class="error-form-validation"><?php echo form_error('address'); ?></span>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-md-6">
						<label for="password">Contraseña:</label>
						<input type="password" class="form-control" name="password" id="password" value="<?php echo set_value('password'); ?>" maxlength="12" placeholder="Contraseña">
						<span class="error-form-validation"><?php echo form_error('password'); ?></span>
					</div>
					<div class="form-group col-md-6">
						<label for="r_password">Repita la Contraseña:</label>
						<input type="password" class="form-control" name="r_password" id="r_password" value="<?php echo set_value('password'); ?>" maxlength="12" placeholder="Repita la Contraseña">
						<span class="error-form-validation"><?php echo form_error('r_password'); ?></span>
					</div>
				</div>
				<a class="btn btn-dark btn-form" href="<?php echo base_url('user')?>"><i class="fas fa-undo"></i> Volver</a>
				<button type="submit" class="btn btn-danger btn-form"><i class="fas fa-user-plus"></i> Agregar</button>
			</form>
		</div>
	</div>
</div>

<div class="container">
	<!-- Imprime un msj de de error en caso de datos incorrectos. -->
	<?php if ($this->session->flashdata("msj-success")):?>
		<div class="row justify-content-center" >
			<div class="alert alert-success text-center content alert-container" role="alert">
				<i class="fas fa-thumbs-up"></i> <strong><?php echo $this->session->flashdata('msj-success'); ?></strong>
			</div>
		</div>
	<?php endif; ?>
</div>

<div class="container">
	<!-- Imprime un msj de de error en caso de datos incorrectos. -->
	<?php if ($this->session->flashdata("msj_error")):?>
		<div class="row justify-content-center" >
			<div class="alert alert-danger text-center content alert-container" role="alert">
				<i class="fas fa-exclamation-triangle"></i> <strong><?php echo $this->session->flashdata('msj_error'); ?></strong>
			</div>
		</div>
	<?php endif; ?>
</div>