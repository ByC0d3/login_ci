<div class="container-fluid">
	<div class="row justify-content-center">
		<form class="form-login" name="form-login" id="form-login" action="<?php echo base_url('login/login')?>" method="post">
			<div class="form-group">
				<input type="text" name="username" id="username" class="form-control form-control-lg text-center" value="<?php echo set_value('username'); ?>" autofocus autocomplete="off" maxlength="12" placeholder="Nombre de Usuario">
					<span class="error" for="username"></span>
					<span class="error-form-validation"><?php echo form_error('username'); ?></span>
			</div>
			<div class="form-group">
				<input type="password" name="password" id="password" class="form-control form-control-lg text-center" maxlength="12" placeholder="Contraseña">
				<span class="error" for="password"></span>
				<span class="error-form-validation"><?php echo form_error('password'); ?></span>
			</div>
			<input type="hidden" value="<?php echo $token;?>" name="token">
			<button type="submit" class="btn btn-dark btn-block">Entrar</button>
		</form>
	</div>
</div>

<div class="container">
	<!-- Imprime un msj de error en caso de datos incorrectos. -->
	<?php if ($this->session->flashdata("msj_error")):?>
		<div class="row justify-content-center" >
			<div class="alert alert-danger text-center content alert-container" role="alert">
				<i class="fas fa-exclamation-triangle"></i> <strong><?php echo $this->session->flashdata('msj_error'); ?></strong>
			</div>
		</div>
	<?php endif; ?>
</div>
